<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\lab\LabController;
use App\Http\Controllers\lab\PasienController as LabPasienController;
use App\Http\Controllers\lab\ResultLabController;
use App\Models\Pasien;
use App\Http\Controllers\rawatjalan\KunjunganController;
use App\Http\Controllers\Pasien\PasienController;
use App\Http\Controllers\RekamMedis\EntryKunjunganController;
use App\Http\Controllers\RekamMedis\RiwayatKunjunganController;
use App\Http\Controllers\RekamMedis\RiwayatMedisController;
use App\Http\Controllers\Pembayaran\PembayaranController;
use App\Http\Controllers\Pharmacy\PharmacyController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
    Route::get('/', 'index')->name('login');
    Route::post('/', 'login')->name('loginPost');
    Route::get('/logout', 'logout')->name('logout');
});

Route::middleware('authenticated')->group(function () {
    // Dashboard Route
    Route::middleware('auth-roles:staff,dokter,admin,teknisi,perawat')->group(function () {
        // Pharmacy Route
        Route::prefix('pharmacy')->name('pharmacy.')->group(function () {
            Route::controller(PharmacyController::class)->group(function () {
                // prescription area
                Route::get('/receipt/{id}', 'getPrescriptionView')->name('prescriptionView');
                Route::get('/receipt/{visitId}/detail/{id}', 'getPrescriptionDetailView')->name('prescriptionDetail');
                Route::post('/receipt/detail', 'createDrugInPrescription')->name('createDrugInPrescription');
                Route::post('/receipt//detail/destroy/{id}', 'destroyDrugInPrescription')->name('destroyDrugInPrescription');
                Route::post('/receipt/detail/update/', 'updateDrugInPrescription')->name('updateDrugInPrescription');
                Route::get('/receipt/checkout/{id}/{visitId}', 'checkoutPrescription')->name('checkoutPrescription');

                // drug area
                Route::get('/medicine', 'listMedicine')->name('listMedicine');
                Route::get('/medicine/{id}', 'detailMedicine')->name('detailMedicine');
                Route::post('/medicine/{id}/destroy', 'destroyDrug')->name('destroyMedicine');
                Route::post('/medicine', 'createDrug')->name('createMedicine');
                Route::post('/medicine/{id}', 'updateDrug')->name('updateMedicine');
                Route::post('/medicine/{id}/update', 'updateDrugStock')->name('updateMedicineStock');
                Route::get('/medicine/export', 'downloadDrugExcel')->name('downloadDrugExcel');
            });
        });


        // Dashboard
        Route::controller(DashboardController::class)->group(function () {
            Route::get('/dashboard', 'index')->name('dashboard');
        });

        // START RJ
        // get data all detail kunjungan RJ
        Route::get('/rawat-jalan/detail-kunjungan', [KunjunganController::class, 'index'])->name('rawatjalan.index');

        // get data pemeriksaan lab and apotek
        Route::get('/rawat-jalan/set-data-pemeriksaan', [KunjunganController::class, 'pemeriksaan'])->name('rawatjalan.pemeriksaan');

        // get url add diagnosa
        Route::get('/rawat-jalan/{detailkunjungan}/add-diagnosa', [KunjunganController::class, 'adddiagnosa'])->name('rawatjalan.adddiagnosa');

        // patch data diagnosa
        Route::patch('/rawat-jalan/{detailkunjungan}/add-diagnosa/success-added', [KunjunganController::class, 'adddiagnosaAdded'])->name('rawatjalan.adddiagnosaAdded');

        // get url add data's (dokter,room,poli)
        Route::get('/rawat-jalan/detail-kunjungan/{detailkunjungan}/add-data-result', [KunjunganController::class, 'show'])->name('rawatjalan.show');

        // patch data results
        Route::patch('/rawat-jalan/detail-kunjungan/{detailkunjungan}/added-data-result', [KunjunganController::class, 'store'])->name('rawatjalan.store');

        // get sorting belum pemeriksaan
        Route::get('/rawat-jalan/detail-kunjungan/belum-pemeriksaan', [KunjunganController::class, 'sortingBelumPemeriksaan'])->name('rawatjalan.belumPemeriksaan');

        // get sorting sudah pemeriksaan
        Route::get('/rawat-jalan/detail-kunjungan/sudah-pemeriksaan', [KunjunganController::class, 'sortingSudahPemeriksaan'])->name('rawatjalan.sudahPemeriksaan');

        // get edit data page n fetch data 
        Route::get('/rawat-jalan/update-kunjungan/{detailkunjungan}/edit-datas', [KunjunganController::class, 'edit'])->name('rawatjalan.edit');

        // Update data
        Route::patch('/rawat-jalan/update-kunjungan/{detailkunjungan}/update-datas', [KunjunganController::class, 'update'])->name('rawatjalan.update');

        // delete data kunjungan 
        Route::delete('rawat-jalan/deleted/{detailkunjungan}/datas', [KunjunganController::class, 'destroy'])->name('rawatjalan.destroy');
    });

    Route::controller(PasienController::class)
        ->prefix('pasien')
        ->name('pasien.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/create', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/edit/{id}', 'edit')->name('edit');
            Route::post('/update/{id}', 'update')->name('update');
            Route::delete('/delete/{id}', 'delete')->name('delete');
        });

    // example when group route with the role
    Route::middleware('auth-roles:admin')->group(function () {
        Route::get('/onlyadmin', function () {
            return 'Only Admin';
        });
        Route::get('/lab', [LabController::class, 'index'])->name('lab.index');
        Route::get('/lab/create', [LabController::class, 'create'])->name('lab.create');
        Route::get('/lab/edit/{lab}', [LabController::class, 'edit'])->name('lab.edit');
        Route::put('/lab/edit/{lab}', [LabController::class, 'update'])->name('lab.update');
        Route::delete('/lab/{lab}', [LabController::class, 'destroy'])->name('lab.destroy');
        Route::post('/lab', [LabController::class, 'store'])->name('lab.store');

        Route::controller(ResultLabController::class)->group(function () {
            Route::get('/result-lab', 'index')->name('resultLab.index');
            Route::get('/result-lab/create', 'create')->name('resultLab.create');
            Route::post('/result-lab', 'store')->name('resultLab.store');
            Route::get('/result-lab/{resultLab}', 'show')->name('resultLab.show');
            Route::delete('/result-lab/{resultLab}', 'destroy')->name('resultLab.destroy');
            Route::get('/result-lab/edit/{resultLab}', 'edit')->name('resultLab.edit');
            Route::put('/result-lab/{resultLab}', 'update')->name('resultLab.update');
            Route::get('/result-lab/download/{resultLab}', 'download')->name('resultLab.download');
        });
    });

    Route::prefix('/pembayaran')->name('pembayaran')->group(function () {
        Route::get('/', [PembayaranController::class, 'index']);

        Route::prefix('/transaksi')->name('-transaksi')->group(function () {
            Route::get('/', [PembayaranController::class, 'transaksi']);
            Route::get('/{page?}', [PembayaranController::class, 'transaksi'])->name('-pagination');
            Route::post('/confirmation/{id?}', [PembayaranController::class, 'update'])->name('-update');
        });

        Route::prefix('/riwayat')->name('-riwayat')->group(function () {
            Route::get('/', [PembayaranController::class, 'riwayat'])->name('-transaksi');
            Route::get('/cetak/{id?}', [PembayaranController::class, 'cetak'])->name('-cetak');
        });
    });
});

Route::view('/example', 'example.example')->name('example');
Route::prefix('rekam-medis')->group(function () {
    Route::controller(EntryKunjunganController::class)
        ->prefix('/entry-kunjungan')
        ->name('entry-kunjungan.')
        ->group(function () {
            Route::get('/', 'showEntryKunjungan')->name('index');
            Route::get('/create', 'createEntryKunjungan')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/{id}', 'details')->name('detail');
        });
    Route::controller(RiwayatKunjunganController::class)
        ->prefix('/riwayat-kunjungan')
        ->name('riwayat-kunjungan.')
        ->group(function () {
            Route::get('/', 'showRiwayatKunjungan')->name('index');
            Route::get('/detail', 'showDetailRiwayatKunjungan')->name('detail');
        });
    Route::controller(RiwayatMedisController::class)
        ->prefix('/riwayat-medis')
        ->name('riwayat-medis.')
        ->group(function () {
            Route::get('/', 'showRiwayatMedis')->name('index');
        });
});
