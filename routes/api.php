<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PasienController;
use App\Http\Controllers\Pembayaran\ApiPembayaranController;

use App\Http\Controllers\Pharmacy\PharmacyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [AuthController::class, 'login'])->name('api.login');

Route::prefix('pharmacy')->name('pharmacy.')->group(function () {
    Route::controller(PharmacyController::class)->group(function () {
        Route::get('/medicine', 'listMedicineAPI')->name('listMedicineAPI');
    });
});



Route::middleware('auth:api')->group(function () {
    Route::controller(PasienController::class)
        ->prefix('pasien')
        ->name('api.pasien.')
        ->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/{id}', 'show')->name('show');
            Route::post('/store', 'store')->name('store');
            Route::post('/update/{id}', 'update')->name('update');
            Route::delete('/delete/{id}', 'delete')->name('delete');
        });

    Route::resource('pembayaran', ApiPembayaranController::class, ['index', 'show']);
});
