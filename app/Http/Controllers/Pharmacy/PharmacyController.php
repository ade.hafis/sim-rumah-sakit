<?php

namespace App\Http\Controllers\Pharmacy;

use App\Exports\DrugsExport;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Pembayaran\PembayaranController;
use App\Models\DetailKunjungan;
use App\Models\Drugs;
use App\Models\PrescriptionDetails;
use App\Models\Prescriptions;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class PharmacyController extends Controller
{

    //------------------------------------------ view section ---------------------------------------------------
    public function listMedicine()
    {
        $drugs =  Drugs::all();
        return view('pharmacy.stok_obat', ['drugs' => $drugs]);
    }

    public function detailMedicine($id)
    {
        $drug = Drugs::where('id', $id)->first();
        return view('pharmacy.detail_obat', ['drug' => $drug]);
    }

    public function getPrescriptionView($id)
    {
        $visitDetail = DetailKunjungan::where('id', $id)->first();
        $prescriptionDetails = $this->getPrescriptionDetail($visitDetail->apotek_id, $visitDetail->id);
        $prescription = $this->getPrescription($id);

        $patient = $visitDetail->kunjungan->pasien;
        $doctor = $visitDetail->user;

        return view('pharmacy.apotek', [
            'visitDetail' => $visitDetail,
            'prescriptionDetails' => $prescriptionDetails,
            'patient' => $patient,
            'doctor' => $doctor,
            'prescription' => $prescription,
            'totalPrice' =>  $this->getTotalBill($prescription->id),
        ]);
    }

    public function getPrescriptionDetailView($visitId, $id)
    {
        $drugs = Drugs::where('stock', '!=', 0)->get();
        return view('pharmacy.detail_apotek', [
            'prescriptionDetails' => $this->getPrescriptionDetail($id, null),
            'drugs' => $drugs,
            'prescriptionId' => $id,
            'visitId' => $visitId
        ]);
    }

    public function downloadDrugExcel()
    {
        $drugs = Drugs::all();
        return Excel::download(new DrugsExport($drugs), 'obat.xlsx');
    }

    //------------------------------------------ destroy section ---------------------------------------------------
    public function destroyPrescriptionDetail($id)
    {
        Prescriptions::destroy($id);
    }

    public function destroyDrug($id)
    {
        Drugs::destroy($id);
        return back();
    }

    public function destroyDrugInPrescription(Request $request, $id)
    {
        $this->operateDrugStock($request->drugId, '+', $request->quantity);
        PrescriptionDetails::destroy($id);
        return back();
    }

    //------------------------------------------ create section ---------------------------------------------------
    public function createDrug(Request $request)
    {
        Drugs::create([
            'name' => $request->name,
            'price' => $request->price,
            'name_code' => Str::uuid(),
            'how_to_use' =>  $request->guide,
            'side_effect' =>  $request->sideEffect,
            'stock' => $request->stock
        ]);

        PembayaranController::updatePengeluaran($request->price * $request->stock);

        return back();
    }

    public function createDrugInPrescription(Request $request)
    {
        $quantity = $this->getAvailableDrugs($request->quantity, $request->medicineId);
        PrescriptionDetails::create([
            'prescription_id' => $request->prescriptionId,
            'drug_id' => $request->medicineId,
            'quantity' => $quantity,
        ]);
        return back();
    }

    //------------------------------------------ update section ---------------------------------------------------
    public function updateDrug(Request $request, $id)
    {
        Drugs::where('id', $id)->update([
            'name' => $request->name,
            'price' => $request->price,
            'how_to_use' =>  $request->guide,
            'side_effect' =>  $request->sideEffect,
        ]);
        return back();
    }

    public function updateDrugStock(Request $request, $id)
    {
        if ($request->addSubstractStock == '+') {
            $this->operateDrugStock($id, '+', $request->stock);
        } else {
            $this->operateDrugStock($id, '-', $request->stock);
        }
        return back();
    }


    public function updateDrugInPrescription(Request $request)
    {
        $isChangeLarger = $request->currentQuantity < $request->quantity;
        $isChangeNotSame = $request->currentQuantity != $request->quantity;
        $value = $this->takeTheDifferentValue($request->quantity, $request->currentQuantity);

        if ($isChangeNotSame) {
            $this->operateDrugStock($request->drugId, $isChangeLarger ? '-' : '+', $value);
            PrescriptionDetails::where('id', $request->prescriptionDetailId)->update([
                'quantity' => $request->quantity,
            ]);
        }
        return back();
    }

    public function checkoutPrescription($id, $visitId)
    {
        $this->processInTransaction($this->getTotalBill($id), $visitId);
        Prescriptions::find($id)->update([
            'is_taken' => 1
        ]);
        return back();
    }

    //------------------------------------------ utils section ---------------------------------------------------

    private function getPrescriptionDetail($prescriptionId, $visitDetailId)
    {
        $prescription = $prescriptionId ?
            Prescriptions::where('id', $prescriptionId)->first() :
            $this->initiatePrescription($visitDetailId);
        return $prescription->prescriptionDetail();
    }

    private function initiatePrescription($visitId)
    {
        $prescription = Prescriptions::create([
            'invoice_code' => Str::uuid()
        ]);
        $this->updateVisitPrescriptionId($visitId, $prescription->id);
        return $prescription;
    }

    private function updateVisitPrescriptionId($visitId, $prescriptionId)
    {
        DetailKunjungan::where('id', $visitId)->update([
            'apotek_id' => $prescriptionId
        ]);
    }

    private function getPrescription($id)
    {
        $prescriptionId = DetailKunjungan::where('id', $id)->first()->apotek_id;
        return Prescriptions::where('id', $prescriptionId)->first();
    }

    private function getAvailableDrugs($qtyRequest, $id)
    {
        $stock = Drugs::where('id', $id)->first()->stock;
        $isEnough = $stock > $qtyRequest;

        Drugs::where('id', $id)->update([
            'stock' => $isEnough ? ($stock - $qtyRequest) : 0
        ]);

        return $isEnough ? $qtyRequest : $stock;
    }

    private function operateDrugStock($id, $operation, $value)
    {
        $currentValue = Drugs::where('id', $id)->first()->stock;
        $currentPrice = Drugs::where('id', $id)->first()->price;

        Drugs::where('id', $id)->update([
            'stock' => ($operation == '+') ? $currentValue + $value : $currentValue - $value,
        ]);

        if ($operation == '+') {
            PembayaranController::updatePengeluaran($currentPrice * $value);
        }
    }

    private function takeTheDifferentValue($value1, $value2)
    {
        $value = $value1 - $value2;
        return ($value < 0) ? (-1 * $value) : $value;
    }

    private function getTotalBill($id)
    {
        $prescriptionDetails = Prescriptions::where('id', $id)->first()->prescriptionDetail();
        $bill = 0;
        foreach ($prescriptionDetails as $prescriptionDetail) {
            $bill += $prescriptionDetail->drug()->price * $prescriptionDetail->quantity;
        }
        return $bill;
    }

    private function processInTransaction($totalBill, $visitId)
    {
        $process = DetailKunjungan::find($visitId);

        if ($process->pembayaran == null) {

            $process->pembayaran = 0 + $totalBill;
        } else {
            $process->pembayaran = $process->pembayaran + $totalBill;
        }

        $process->save();
    }

    public function listMedicineAPI()
    {
        $drugs =  Drugs::all();
        // return view('pharmacy.stok_obat', ['drugs' => $drugs]);
        return response()->json($drugs);
    }
}
