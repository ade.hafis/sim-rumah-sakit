<?php

namespace App\Http\Controllers\Pembayaran;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;

class ApiPembayaranController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice = Invoice::whereHas('dkunjungan.kunjungan')->with('dkunjungan')->latest()->paginate();
        
        if($invoice == null){
            return response()->json([
               'pesan' => 'Data Tidak Ditemukan' 
            ],400);
        }

        return response()->json([
            'data' => $invoice,
            'pesan' => 'Data Ditemukan'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::whereHas('dkunjungan.kunjungan')
            ->with('dkunjungan')
            ->where('id', $id)
            ->first();
        if($invoice == null){
            return response()->json([
               'pesan' => 'Data Tidak Ditemukan' 
            ],400);
        }

        return response()->json([
            'data' => $invoice,
            'pesan' => 'Data Ditemukan'
        ],200);
    }

}
