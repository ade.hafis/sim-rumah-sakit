<?php

namespace App\Http\Controllers\Pembayaran;

use App\Http\Controllers\Controller;
use App\Models\DetailKunjungan;
use App\Models\Drugs;
use App\Models\Invoice;
use App\Models\Keuangan;
use App\Models\Kunjungan;
use App\Models\PrescriptionDetails;
use App\Models\Prescriptions;
use App\Models\ResultLab;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Psy\Exception\ThrowUpException;

class PembayaranController extends Controller
{
    private Kunjungan $kunjungan;



    public function index(){
        $totalIncome = Keuangan::whereYear('tanggal_arsip', now()->year)
            ->sum('pemasukan');


        $totalOutcome = Keuangan::whereYear('tanggal_arsip', now()->year)
            ->sum('pengeluaran');
        
        $incomeMonth = [];
        foreach(range(1,12) as $month){
            $income = Keuangan::whereMonth('tanggal_arsip',$month)->sum('pemasukan');
            $incomeMonth[$month] = $income;
        }
        $outcomeMonth = [];
        foreach(range(1,12) as $month){
            $outcome = Keuangan::whereMonth('tanggal_arsip',$month)->sum('pengeluaran');
            $outcomeMonth[$month] = $outcome;
        }
        
        $keuangan = Keuangan::all();

        return view('pembayaran.index',compact('keuangan','totalIncome','totalOutcome','incomeMonth','outcomeMonth'));
    }
    public static function updatePengeluaran($duit)
    {
        $keuangan = new Keuangan();

        $keuangan->pemasukan = 0;
        $keuangan->pengeluaran = $duit;
        $keuangan->tanggal_arsip = now();
        $keuangan->save();

    }

    public function updateKeuangan($id){
        $keuangan = new Keuangan();
        
        $invoice = Invoice::whereHas('dkunjungan', function ($query) use ($id) {
            $query->where('id', $id);
        })->first();

        if (!$invoice) {
            return response()->json(['message' => 'Invoice not found.'], 404);
        }

        if($invoice->dkunjungan->pembayaran == null){
            $keuangan->pemasukan = 0 + 30000;
        }else{
            $keuangan->pemasukan = $invoice->dkunjungan->pembayaran;
        }
        // dd($invoice->dkunjungan->pembayaran);

        $keuangan->pengeluaran = 0;
        $keuangan->tanggal_arsip = now();
        $keuangan->save();

        return response()->json(['message' => 'Pemasukan berhasil dicatat.']);
    }


    public function transaksi()
    {
        $invoice = Invoice::whereHas('dkunjungan.kunjungan', function ($query) {
            $query->where('status_pembayaran', false);
        })->with('dkunjungan')->latest()->paginate(10);
        
        $apotek = PrescriptionDetails::all();
        $drugs = Drugs::all();
        $lab = ResultLab::all();

        return view('pembayaran.transaksi',compact('invoice','apotek','drugs','lab'));
    }

    public function riwayat(){
        $invoice = Invoice::whereHas('dkunjungan.kunjungan', function ($query) {
            $query->where('status_pembayaran', true);
        })->with('dkunjungan')->latest()->paginate(10);

        $apotek = PrescriptionDetails::all();
        $drugs = Drugs::all();
        $lab = ResultLab::all();
        
        return view('pembayaran.riwayattransaksi',compact('invoice','apotek','drugs','lab'));

    }

    public function cetak(Request $request, $id){
        
        try{
            $invoice = Invoice::whereHas('dkunjungan.kunjungan', function ($query) {
                $query->where('status_pembayaran', true);
            })
            ->with('dkunjungan')
            ->where('id', $id)
            ->first();

            $apotek = PrescriptionDetails::all();
            $drugs = Drugs::all();
            $lab = ResultLab::all();

            
            if($invoice == null){
                throw new Exception('eww');
            }
            return view('pembayaran.assets.nota', compact('invoice','apotek','drugs','lab'));
        } catch (\Throwable $err){
            return redirect('pembayaran/riwayat');
        }

    }


    public function update(Request $request, $id){
        $this->kunjungan = new Kunjungan();

        $validated = $request->validate([
            'status_pembayaran' => 'required',
        ]);
        // dd($this->kunjungan);
        try{
            $this->kunjungan->find($id)->update($validated);
            
            $this->updateKeuangan($id);
            return redirect()->back()->with('alert', 'Updated!');

        }catch (\Throwable $err) {
            return response()->json([
                'status' => 'error',
                'message' => $err->getMessage()
            ]);
        };


    }




}
