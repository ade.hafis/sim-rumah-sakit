<?php

namespace App\Http\Controllers\Pasien;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pasien;

class PasienController extends Controller
{
    private Pasien $pasien;

    public function __construct(Pasien $pasien)
    {
        $this->pasien = $pasien;
    }

    public function index(){
        $pasien = $this->pasien->all();
        return view('pasien.index', compact('pasien'));
    }

    public function create(){
        return view('pasien.create');
    }

    public function store(Request $request){

        $validated = $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required|numeric',
            'jenis_kelamin' => 'required|in:L,P',
            'tanggal_lahir' => 'required|date',
            'no_ktp' => 'required',
            'no_bpjs' => 'required',
        ]);

        try {
            $this->pasien->create($validated);
            return response()->json([
                'status' => 'success',
                'message' => 'Pasien berhasil ditambahkan'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => $th->getMessage()
            ], 500);
        }

    }


    public function edit($id){
        $pasien = $this->pasien->find($id);
        return view('pasien.edit', compact('pasien'));
    }

    public function delete($id){
        try {
            $this->pasien->find($id)->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'Pasien berhasil dihapus'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => 'Pasien gagal dihapus'
            ]);
        }
    }

    public function update(Request $request, $id){
        $validated = $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required|numeric',
            'jenis_kelamin' => 'required|in:L,P',
            'tanggal_lahir' => 'required|date',
            'no_ktp' => 'required',
            'no_bpjs' => 'required',
        ]);

        try {
            $this->pasien->find($id)->update($validated);
            return response()->json([
                'status' => 'success',
                'message' => 'Pasien berhasil diupdate'
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => 'Pasien gagal diupdate'
            ]);
        }
    }

}
