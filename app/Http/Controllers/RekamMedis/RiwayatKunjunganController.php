<?php

namespace App\Http\Controllers\RekamMedis;

use App\Http\Controllers\Controller;

class RiwayatKunjunganController extends Controller
{
    public function showRiwayatKunjungan()
    {
        return view('rekam-medis.riwayat-kunjungan.index');
    }
    public function showDetailRiwayatKunjungan()
    {
        return view('rekam-medis.riwayat-kunjungan.detail');
    }
}
