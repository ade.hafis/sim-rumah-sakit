<?php

namespace App\Http\Controllers\RekamMedis;

use App\Http\Controllers\Controller;
use App\Models\Kunjungan;
use App\Models\Pasien;
use Illuminate\Http\Request;
use App\Models\DetailKunjungan;
use App\Models\Invoice;

class EntryKunjunganController extends Controller
{
    private Kunjungan $kunjungan;
    private Pasien $pasien;
    private DetailKunjungan $detailKunjungan;
    private Invoice $invoices;

    public function __construct(Kunjungan $kunjungan, Pasien $pasien, DetailKunjungan $detailKunjungan, Invoice $invoices)
    {
        $this->kunjungan = $kunjungan;
        $this->pasien = $pasien;
        $this->detailKunjungan = $detailKunjungan;
        $this->invoices = $invoices;
    }
    public function showEntryKunjungan()
    {
        return view('rekam-medis.entry-kunjungan.index', [
            'kunjungan' => $this->kunjungan->all()
        ]);
    }
    public function createEntryKunjungan()
    {
        return view('rekam-medis.entry-kunjungan.create', [
            'pasien' => $this->pasien->all()
        ]);
    }
    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'pasien_id' => 'required|exists:pasiens,id',
                'tanggal_kunjungan' => 'required|date',
                'keluhan' => 'required|string',
            ]);


            $kunjungan = $this->kunjungan->create($validated);
            $res = $this->detailKunjungan->create([
                'kunjungan_id' => $kunjungan->id,
                'pembayaran' => 30000,
            ]);

            // dd($this->detailKunjungan);
            $this->invoices->create([
                'id_detail_kunjungan' => $res->id,
            ]);

            return response()->json([
                'message' => 'Berhasil menambahkan data kunjungan'
            ], 200);
        } catch (\Exception $error) {
            return response()->json([
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function details($id)
    {
        if (!$this->kunjungan->find($id)) {
            return response()->json([
                'message' => 'Data kunjungan tidak ditemukan'
            ], 404);
        }
        return view('rekam-medis.entry-kunjungan.details', [
            'pasien' => $this->kunjungan->find($id),
            'kunjungan' => $this->detailKunjungan->where('kunjungan_id', $id)->get(),
            'jumlah_kunjungan' => $this->detailKunjungan->where('kunjungan_id', $id)->count()
        ]);
    }
}
