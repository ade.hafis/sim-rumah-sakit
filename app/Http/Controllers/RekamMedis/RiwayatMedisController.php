<?php

namespace App\Http\Controllers\RekamMedis;

use App\Http\Controllers\Controller;

class RiwayatMedisController extends Controller
{
    public function showRiwayatMedis()
    {
        return view('rekam-medis.riwayat-medis.index');
    }
}
