<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Kunjungan;
use App\Models\Pasien;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        // $kunjungan = Kunjungan::whereMonth('tanggal_kunjungan',5)->count();

        $data = [];
        foreach (range(1, 12) as $month) {
            $kunjungan = Kunjungan::whereMonth('tanggal_kunjungan', $month)->count();
            $data[$month] = $kunjungan;
        }

        $datas = [];

        $pasien = Pasien::all()->count();
        $user = User::where('roles', 'dokter')->count();
        $lain = User::where('roles', '!=', 'dokter')->count();

        $datas[0] = $pasien;
        $datas[1] = $user;
        $datas[2] = $lain;

        return view('dashboard.dashboard', compact('data', 'datas'));
    }
}
