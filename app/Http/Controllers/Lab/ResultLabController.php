<?php

namespace App\Http\Controllers\lab;

use App\Models\Lab;
use App\Models\User;
use App\Models\Pasien;
use App\Models\Kunjungan;
use App\Models\ResultLab;
use Illuminate\Http\Request;
use App\Models\DetailKunjungan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ResultLabController extends Controller
{
    public function index()
    {
        $resultLabs = ResultLab::with(['lab', 'user', 'kunjungan'])->orderBy('id', 'desc')->get();

        return view('lab.pasiens', compact('resultLabs'));
    }

    public function create()
    {
        $dokters = User::where('roles', 'dokter')->orderBy('name', 'asc')->get();
        $kunjungans = Kunjungan::orderBy('id', 'asc')->get();
        $labs = Lab::orderBy('name', 'asc')->get();
        return view('lab.create_pasien', compact('kunjungans', 'dokters', 'labs'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            "user_id" => 'required',
            "lab_id" => 'required',
            "kunjungan_id" => 'required'
        ]);

        $resultLab = ResultLab::create($validated);

        return redirect('/result-lab')->with('success', "Berhasil mendaftarkan pasien");
    }

    public function show(ResultLab $resultLab)
    {
        return view('lab.detail_pasien', compact('resultLab'));
    }

    public function destroy(ResultLab $resultLab)
    {
        if ($resultLab->hasil_lab) {
            Storage::delete($resultLab->hasil_lab);
        }
        $resultLab->delete();
        return redirect('/result-lab')->with('success', 'Berhasil menghapus pasien');
    }
    public function edit(ResultLab $resultLab)
    {
        $dokters = User::where('roles', 'dokter')->orderBy('name', 'asc')->get();
        $kunjungans = Kunjungan::orderBy('id', 'asc')->get();
        $labs = Lab::orderBy('name', 'asc')->get();
        return view('lab.edit_pasien', compact('resultLab', 'kunjungans', 'dokters', 'labs'));
    }

    public function update(Request $request, ResultLab $resultLab)
    {
        $validated = $request->validate([
            'kunjungan_id' => 'required',
            'user_id' => 'required',
            'lab_id' => 'required',
            'hasil_lab' => 'required|mimes:pdf',
            'status' => 'required',
            'description' => 'nullable',
        ]);

        if ($resultLab->hasil_lab && $request->file('hasil_lab')) {
            Storage::delete($resultLab->hasil_lab);
        }

        $validated['hasil_lab'] = $request->file('hasil_lab')->store('public/hasil-lab');

        
        if($validated['status'] === 'success' && $resultLab->status == null){
            
            $invoiceUpdate = DetailKunjungan::where('kunjungan_id',$resultLab->kunjungan_id)->first();
            
            $invoiceUpdate->pembayaran = $invoiceUpdate->pembayaran + $resultLab->lab->price;
            $invoiceUpdate->update();
        }

        $resultLab->update($validated);

        return redirect('/result-lab')->with("success", "Berhasil mengupdate data");
    }

    public function download(ResultLab $resultLab)
    {
        return Storage::download($resultLab->hasil_lab);
    }
}
