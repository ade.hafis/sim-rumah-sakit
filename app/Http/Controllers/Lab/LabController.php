<?php

namespace App\Http\Controllers\lab;

use App\Models\Lab;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LabController extends Controller
{
    public function index()
    {
        $labs = Lab::all();
        return view('lab.index', compact('labs'));
    }

    public function create()
    {
        return view('lab.create_lab');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'code_lab' => 'required|string|unique:labs',
            'name' => 'required|string|max:255',
            'price' => 'required|integer',
            'description' => 'required|string|max:255',
        ]);

        Lab::create($validated);

        return redirect('/lab')->with('success', 'berhasil menambahkan data');
    }

    public function edit(Lab $lab)
    {
        return view('lab.edit_lab', compact('lab'));
    }

    public function update(Request $request, Lab $lab)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'price' => 'required|integer',
            'description' => 'required|string|max:255',
        ]);

        if ($request->code_lab != $lab->code_lab) {
            $validated = $request->validate([
                'code_lab' => 'required|string|max:10|unique:labs',
            ]);
        }

        $lab->update($request->all());

        return redirect('/lab')->with('success', 'Berhasil mengubah data');
    }

    public function destroy(Lab $lab)
    {
        $lab->delete();
        return redirect('/lab')->with('success', 'Berhasil Menghapus data');
    }
}
