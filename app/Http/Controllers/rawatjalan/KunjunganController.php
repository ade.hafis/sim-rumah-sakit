<?php

namespace App\Http\Controllers\rawatjalan;

use App\Http\Controllers\Controller;
use App\Models\DetailKunjungan;
use App\Models\Poli;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;

class KunjunganController extends Controller
{
    public function index()
    {
        $detailkunjungans = DetailKunjungan::all();

        return view('rawatjalan.detail-rawat-jalan', compact('detailkunjungans'));
    }

    public function adddiagnosa(DetailKunjungan $detailkunjungan)
    {
        return view('rawatjalan.add-diagnosa', compact('detailkunjungan'));
    }

    
    public function adddiagnosaAdded(Request $request , DetailKunjungan $detailkunjungan)
    {
        $request->validate([
            'diagnosa' => ['required'],
            'resep' => ['required'],
        ]);

        $detailkunjungan->update([
            'diagnosa' => $request->diagnosa,
            'resep' => $request->resep,
        ]);

        return redirect('rawat-jalan/detail-kunjungan')->with('success', 'Diagnosis has been successfully added');

    }

    public function sortingSudahPemeriksaan(){
        $detailkunjungans = DetailKunjungan::whereNotNull('diagnosa')->get();
        
        return view('rawatjalan.detail-rawat-jalan', compact('detailkunjungans')); 
    }
    
    public function sortingBelumPemeriksaan(){
        $detailkunjungans = DetailKunjungan::whereNull('diagnosa')->get();
        
        return view('rawatjalan.detail-rawat-jalan', compact('detailkunjungans')); 
    }

    public function show(DetailKunjungan $detailkunjungan){
        $rooms = Room::get();
        $dokters = User::where('roles', 'dokter')->get();
        $dokterse = User::where('roles', 'perawat')->get();
        $polis = Poli::get();
        return view('rawatjalan.add-jadwal-pasien', compact('detailkunjungan', 'rooms', 'dokters', 'polis', 'dokterse')); 
    }


    public function store(Request $request, DetailKunjungan $detailkunjungan){
        $request->validate([
            'user_id' => ['required'],
            'poli_id' => ['required'],
        ]);

        $detailkunjungan->update([
            'user_id' => $request->user_id,
            'room_id' => $request->room_id,
            'poli_id' => $request->poli_id,
        ]);


        return redirect('/rawat-jalan/detail-kunjungan')->with('success', 'The Room And Dokter Has Been Adddeed');
    }
    
    public function edit(DetailKunjungan $detailkunjungan){
        $dokters = User::where('roles','dokter')->get();
        $dokterse = User::where('roles','perawat')->get();
        $polis = Poli::get();
        $rooms = Room::get();
        return view('rawatjalan.update-detail', compact('detailkunjungan','dokters', 'polis', 'rooms','dokterse')); 
    }

    public function update(Request $request, DetailKunjungan $detailkunjungan)
    {

    $detailkunjungan->update([
        'user_id' => $request->user_id,
        'diagnosa' => $request->diagnosa,
        'resep' => $request->resep,
        'poli_id' => $request->poli_id,
        'room_id' => $request->room_id,
    ]);

    return redirect('/rawat-jalan/detail-kunjungan')->with('success', 'Visit details updated successfully');
    
    }

    public function pemeriksaan()
    {
        $detailkunjungans = DetailKunjungan::get();

        return view('rawatjalan.set-data-pemeriksaan', compact('detailkunjungans'));
    }

    public function destroy(DetailKunjungan $detailkunjungan){
        $detailkunjungan->delete();

        return back()->with('success', 'The Datas Has Been Deleteed');
    }

}
