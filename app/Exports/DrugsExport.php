<?php

namespace App\Exports;

use App\Models\Drugs;
use Maatwebsite\Excel\Concerns\FromCollection;

class DrugsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Drugs::all();
    }
}
