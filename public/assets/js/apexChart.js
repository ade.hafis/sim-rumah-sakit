

var options = {
  series: [{
  name: 'Pemasukan',
  data: [
    0,
    {!! json_encode($totalIncome, JSON_HEX_TAG) !!}
  ]
  
}, {
  name: 'Pengeluaran',
  data: [
    {!! json_encode($totalOutcome, JSON_HEX_TAG) !!}, {!! json_encode($totalIncome, JSON_HEX_TAG) !!}
  ]
}],
  chart: {
  height: 350,
  type: 'area'
},
dataLabels: {
  enabled: false
},
stroke: {
  curve: 'smooth'
},
xaxis: {
  type: 'datetime',
  categories: ["2023-01-01T00:00:00.000Z", "2023-12-01T00:00:00.000Z"]
},
tooltip: {
  x: {
    format: 'dd/MM/yy HH:mm'
  },
},
};

var chart = new ApexCharts(document.querySelector("#keuanganChart"), options);
chart.render();
