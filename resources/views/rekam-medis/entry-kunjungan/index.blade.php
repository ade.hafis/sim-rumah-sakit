@extends('templates.master')
@section('title', 'Rekam Medis')
@section('page-name', 'Entry Kunjungan')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
@endpush
@section('content')
    <div class="page-heading">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Kunjungan Pasien
                    </h5>
                    <a class="btn btn-primary" href="{{ route('entry-kunjungan.create') }}">
                        <i class="bi bi-plus"></i>Data
                    </a>
                    <span class="float-end" id="waktu-kunjungan"></span>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>No Kunjungan</th>
                            <th>Nama Pasien</th>
                            <th>No. BPJS</th>
                            <th>Tanggal Kunjungan</th>
                            <th>Detail</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($kunjungan as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->pasien->nama }}</td>
                                <td>{{ $item->pasien->no_bpjs }}</td>
                                <td>{{ date('d F Y', strtotime($item->tanggal_kunjungan)) }}</td>
                                <td>
                                    <a href="{{ route('entry-kunjungan.detail', $item->id) }}" class="btn btn-primary">
                                        <i class="bi bi-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
@endsection
@push('scripts')
    <script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script src={{ asset ('https://code.jquery.com/jquery-3.6.0.min.js') }}></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )
        $(document).ready(function () {
            function updateWaktuKunjungan() {
                var options = {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                };
                var waktuSaatIni = new Date().toLocaleDateString('id-ID', options);
                $("#waktu-kunjungan").text("Hari : " + waktuSaatIni);
            }

            setInterval(updateWaktuKunjungan, 1000);
        });
    </script>
@endpush

