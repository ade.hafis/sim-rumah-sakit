@extends('templates.master')
@section('title', 'Tambah Kunjungan')
@section('page-name', 'Tambah Kunjungan')
@push('styles')
@endpush
@section('content')
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"></h4>
        </div>
        <div class="card-body">
            <form action="{{ route('entry-kunjungan.create') }}" id="form">
                <div class="form-grpup">
                    <label for="pasien">Daftar Pasien</label>
                    <select name="pasien_id" id="pasien" class="form-control">
                        <option value="" selected disabled>-- Nama Pasien --</option>
                        @foreach ($pasien as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="tanggal_kunjungan">Tanggal Kunjungan</label>
                    <input type="date" name="tanggal_kunjungan" id="tanggal_kunjungan" class="form-control">
                </div>
                <div class="form-group">
                    <label for="keluhan">Keluhan</label>
                    <textarea name="keluhan" id="" cols="30" rows="5" class="form-control"></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script>
        $('#form').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                method: "POST",
                data: $(this).serialize(),
                url: "{{ route('entry-kunjungan.store') }}",
                success: function(response) {
                    swal({
                        title: "Success",
                        text: response.message,
                        icon: "success",
                    });
                    $('#form').trigger('reset');
                },
                error: function(xhr, status, error) {
                    swal({
                        title: "Error",
                        text: xhr.responseJSON.message,
                        icon: "error",
                    });
                }
            })
        })
    </script>
@endpush
