@extends('templates.master')
@section('title', 'Rekam Medis')
@section('page-name', 'Detail Riwayat Kunjungan')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
    <link rel="stylesheet" href="{{ asset('assets/css/custome/style.css') }}">
@endpush
@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <h5 class="card-title">Biodata Pasien</h5>
                <p class="card-text">
                    Riwayat kunjungan dan informasi rekam medis pasien
                </p>
            </div>
        </div>
        <div>
            <div class="container list">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><span class="label">Nama </span>: {{ $pasien->pasien->nama }}</li>
                    <li class="list-group-item"><span class="label">Alamat </span>: {{ $pasien->pasien->alamat }}</li>
                    <li class="list-group-item"><span class="label">Tanggal kunjungan </span>: {{ $pasien->tanggal_kunjungan }}</li>
                    <li class="list-group-item"><span class="label">Tanggal Lahir </span>: {{ date('d F Y', strtotime($pasien->tanggal_lahir)) }}
                    <li class="list-group-item"><span class="label">No. KTP </span>: {{ $pasien->pasien->no_ktp }}</li>
                    <li class="list-group-item"><span class="label">No. BPJS </span>: {{ $pasien->pasien->no_bpjs }}</li>
                    <li class="list-group-item"><span class="label">Jumlah Kunjungan </span>: {{ $jumlah_kunjungan }}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page-heading">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Riwayat Medis
                    </h5>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>Tanggal Kunjungan</th>
                            <th>Tujuan Kunjungan</th>
                            <th>Keluhan</th>
                            <th>Resep Obat</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($kunjungan as $item)
                            <tr>
                                <td>{{ date('d F Y', strtotime($item->kunjungan->tanggal_kunjungan)) }}</td>
                                <td>
                                    @if(!is_null($item->poli_id))
                                        Rawat Jalan
                                    @elseif(!is_null($item->apotek_id))
                                        Apotek
                                    @elseif(!is_null($item->lab_id))
                                        Laboratorium
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{ $item->kunjungan->keluhan }}</td>
                                <td>{{ $item->diagnosa }}</td>
                                <td>{{ $item->resep }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
@endsection
@push('scripts')
    <script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )
    </script>
@endpush
