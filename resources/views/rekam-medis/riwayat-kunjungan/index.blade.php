@extends('templates.master')
@section('title', 'Rekam Medis')
@section('page-name', 'Riwayat Kunjungan')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
@endpush
@section('content')
    <div class="page-heading">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Riwayat Kunjungan Pasien
                    </h5>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>No Kunjungan</th>
                            <th>Nama Pasien</th>
                            <th>No. BPJS</th>
                            <th>Tanggal Kunjungan</th>
                            <th>Detail</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>0123</td>
                            <td>vehicula</td>
                            <td>076 4820 8838</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0124</td>
                            <td>velit</td>
                            <td>0309 690 7871</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0125</td>
                            <td>rhoncus</td>
                            <td>0500 441046</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0126</td>
                            <td>marcel</td>
                            <td>27844</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0127</td>
                            <td>sodale</td>
                            <td>0800 528324</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0128</td>
                            <td>Duis</td>
                            <td>07740 599321</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0129</td>
                            <td>Donec</td>
                            <td>0845 46 49</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0130</td>
                            <td>jasmine</td>
                            <td>0800 1111</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0131</td>
                            <td>ray</td>
                            <td>896 6829</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0132</td>
                            <td>dapibus</td>
                            <td>0334 836 4028</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0133</td>
                            <td>fringilla</td>
                            <td>043 2822</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0134</td>
                            <td>philip</td>
                            <td>0500 571108</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>0135</td>
                            <td>justo</td>
                            <td>07624 682306</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>
                                <a class="btn btn-primary" href="{{ route('riwayat-kunjungan.detail') }}">
                                    <i class="bi bi-eye-fill"></i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
@endsection
@push('scripts')
    <script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )
    </script>
@endpush

