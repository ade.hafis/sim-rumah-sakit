@extends('templates.master')
@section('title', 'Rekam Medis')
@section('page-name', 'Detail Riwayat Kunjungan')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
    <link rel="stylesheet" href="{{ asset('assets/css/custome/style.css') }}">
@endpush
@section('content')
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <h5 class="card-title">Biodata Pasien</h5>
                <p class="card-text">
                    Riwayat kunjungan dan informasi rekam medis pasien
                </p>
            </div>
        </div>
        <div>
            <div class="container list">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><span class="label">Nama </span>: Nama</li>
                    <li class="list-group-item"><span class="label">Alamat </span>: Jalan Contoh 123</li>
                    <li class="list-group-item"><span class="label">Tanggal Lahir </span>: 01 Januari 1990</li>
                    <li class="list-group-item"><span class="label">No. KTP </span>: 1234567890</li>
                    <li class="list-group-item"><span class="label">No. BPJS </span>: 076 4820 8838</li>
                    <li class="list-group-item"><span class="label">Jumlah Kunjungan </span>: 2</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page-heading">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Riwayat Medis
                    </h5>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>Tanggal Kunjungan</th>
                            <th>Keluhan</th>
                            <th>Diagnosa</th>
                            <th>Resep Obat</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Sakit Hati</td>
                            <td>Liver</td>
                            <td>Paracetamol</td>
                        </tr>
                        <tr>
                            <td>Senin, 02 Oktober 2023</td>
                            <td>Mimisan</td>
                            <td>Kanker</td>
                            <td>Antimo</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
@endsection
@push('scripts')
    <script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )
    </script>
@endpush
