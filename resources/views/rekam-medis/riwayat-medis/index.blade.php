@extends('templates.master')
@section('title', 'Rekam Medis')
@section('page-name', 'Riwayat Medis')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
@endpush
@section('content')
    <div class="page-heading">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Riwayat Medis
                    </h5>
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>Nama Pasien</th>
                            <th>Tanggal Kunjungan</th>
                            <th>Keluhan</th>
                            <th>No. BPJS</th>
                            <th>Detail</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>vehicula</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Sakit Hati</td>
                            <td>076 4820 8838</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>velit</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Darah Rendah</td>
                            <td>0309 690 7871</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>rhoncus</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Mimisan</td>
                            <td>0500 441046</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>marcel</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Sariawan</td>
                            <td>27844</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>sodale</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Panu</td>
                            <td>0800 528324</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>Duis</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Kanker</td>
                            <td>07740 599321</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>Donec</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Kelaparan</td>
                            <td>0845 46 49</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>jasmine</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Gila</td>
                            <td>0800 1111</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>ray</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Sinusitis</td>
                            <td>896 6829</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>dapibus</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Gerd</td>
                            <td>0334 836 4028</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>fringilla</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Dehidrasi</td>
                            <td>043 2822</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>philip</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Alergi</td>
                            <td>0500 571108</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        <tr>
                            <td>justo</td>
                            <td>Sabtu, 30 September 2023</td>
                            <td>Pilek</td>
                            <td>07624 682306</td>
                            <td>
                                <btn class="btn btn-primary"><i class="bi bi-eye-fill"></i></btn>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
@endsection
@push('scripts')
    <script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )
    </script>
@endpush

