@extends('templates.master')
@section('title', 'Rawat Jalan')
@section('page-name', 'Add Diagnosa')
@push('styles')
@endpush
@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="container">
                <div class="card-body">
                    <form action="{{ route('rawatjalan.adddiagnosaAdded', $detailkunjungan->id) }}" method="post">
                        @csrf
                        @method('patch')
                        <div class="form-group mb-3">
                            <h6 class="card-title" style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Diagnosa<span style="color: red;" > *</span></h6>
                            <textarea class="form-control" name="diagnosa" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        @error('diagnosa')
                            <p class="text-danger">
                                {{ $message }}
                            </p>
                        @enderror
                        <div class="form-group mb-3">
                            <h6 class="card-title" style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Resep<span style="color: red;" > *</span></h6>
                            <textarea class="form-control" name="resep" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        @error('resep')
                            <p class="text-danger">
                                {{ $message }}
                            </p>
                        @enderror
                        <br>
                        <p style="font-size: 14px; margin-top: 7px; font-weight: bold; color: red; font-family: 'Montserrat', sans-serif;">
                            <i>*tambahkan diagnosa dan resep sesuai kebutuhan pasien</i>
                        </p>
                        <div class="col-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary me-1 mb-1 mt-5">
                                <i class="fa-regular fa-floppy-disk">
                                </i> Simpan
                            </button>
                        </div>
                    </form>            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush