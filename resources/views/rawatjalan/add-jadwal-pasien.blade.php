@extends('templates.master')
@section('title', 'Rawat Jalan')
@section('page-name', 'Add Data Pasien')
@push('styles')
@endpush
@section('content')
<div class="col-md-12 col-12">
    <form action="{{ route('rawatjalan.store', $detailkunjungan) }}" method="post">
        @csrf
        @method('patch')
        <div class="card">
            <div class="container">
                <div class="card-content">
                    <div class="card-body">
                        <form class="form form-horizontal">
                            <div class="form-body">
                                <div class="row">
                                    <!-- Nama Pasien -->
                                    <div class="col-md-5">
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Nama
                                                Pasien<span style="color: red;"> *</span></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-7 mb-4">
                                        <p>{{ $detailkunjungan->kunjungan->pasien->nama }}</p>
                                    </div>

                                    <!-- Dokter -->
                                    @if($detailkunjungan->user_id == null)

                                    <div class="col-md-5">
                                        <div class="form-group mb-3 mt-2">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Dokter<span style="color: red;"> </span></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <fieldset class="form-group">
                                            <select class="form-select" id="basicSelect" name="user_id">
                                                <option value="" selected disabled>Pilih dokter untuk pasien</option>
                                                @foreach ($dokters as $dokter)
                                                <option value="{{ $dokter->id }}">#000{{ $dokter->id }} - {{
                                                    $dokter->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('user_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </fieldset>
                                    </div>


                                    <div class="col-md-5 mt-2">
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Perawat
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <fieldset class="form-group">
                                            <select class="form-select" id="basicSelect" name="user_id">
                                                <option value="" selected disabled>Pilih perawat untuk pasien</option>
                                                @foreach ($dokterse as $perawat)
                                                <option value="{{ $perawat->id }}"> {{ $perawat->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('user_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </fieldset>
                                    </div>
                                    {{-- @endif


                                    @else
                                    <input type="hidden" name="user_id" value="{{ $detailkunjungan->user_id }}">
                                    @endif --}}
                                    @else
                                    @if($detailkunjungan->user->roles == 'perawat')
                                    <div class="col-md-5">
                                        <div class="form-group mb-3 mt-2">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Dokter<span style="color: red;"> *</span></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <fieldset class="form-group">
                                            <select class="form-select" id="basicSelect" name="user_id">
                                                <option value="" selected disabled>Pilih dokter untuk pasien</option>
                                                @foreach ($dokters as $dokter)
                                                <option value="{{ $dokter->id }}">#000{{ $dokter->id }} - {{
                                                    $dokter->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('user_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </fieldset>
                                    </div>
                                    @else
                                    <input type="hidden" name="user_id" value="{{ $detailkunjungan->user_id }}">
                                    @endif
                                   
                                   
                                    @endif

                                    @if($detailkunjungan->user_id != null)
                                    <!-- Room -->
                                    <div class="col-md-5 mt-2">
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Room
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <fieldset class="form-group">
                                            <select class="form-select" id="basicSelect" name="room_id">
                                                <option value="" selected disabled>Pilih room untuk pasien</option>
                                                @foreach ($rooms as $room)
                                                <option value="{{ $room->id }}">{{ $room->no_room }} - {{
                                                    $room->name_room }}</option>
                                                @endforeach
                                            </select>
                                            @error('room_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </fieldset>
                                    </div>

                                    @else
                                    <div class="col-md-5 mt-2">
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Room
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <fieldset class="form-group">
                                            <select class="form-select" id="basicSelect" name="room_id">
                                                <option value="" selected disabled>Pilih room untuk pasien</option>
                                                @foreach ($rooms as $room)
                                                <option value="{{ $room->id }}">{{ $room->no_room }} - {{
                                                    $room->name_room }}</option>
                                                @endforeach
                                            </select>
                                            @error('room_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </fieldset>
                                    </div>
                                    @endif

                                    <!-- Poli -->
                                    @if($detailkunjungan->poli_id == null)
                                    <div class="col-md-5 mt-2">
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                Poli<span style="color: red;"> *</span></h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <fieldset class="form-group" style="min-width: 200px;">
                                            <select class="form-select" id="basicSelect" name="poli_id">
                                                <option value="" selected disabled>Pilih identifier Poli</option>
                                                @foreach ($polis as $poli)
                                                <option value="{{ $poli->id }}">{{ $poli->name_poli }}</option>
                                                @endforeach
                                            </select>
                                            @error('poli_id')
                                            <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </fieldset>
                                    </div>
                                    @else
                                    <input type="hidden" name="poli_id" value="{{ $detailkunjungan->poli_id }}">
                                    @endif
                                </div>
                                <br>
                                <div class="col-sm-12 d-flex justify-content-end mt-5 mb-5">
                                    <a class="btn btn-light-secondary me-3 mb-1"
                                        href="{{ route('rawatjalan.index') }}">Kembali</a>
                                    <button type="submit" class="btn btn-primary me-1 mb-1"><i
                                            class="fa-regular fa-floppy-disk"></i> Simpan</button>
                                </div>
                            </div>
                    </div>
    </form>
</div>
</div>
</div>
</div>
</form>
</div>
@endsection
@push('scripts')
@endpush