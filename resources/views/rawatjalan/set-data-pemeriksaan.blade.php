@extends('templates.master')
@section('title', 'Set Data Pemeriksaan')
@section('page-name', 'Set Data Pemeriksaaan')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
                <table class="table table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>ID Kunjungan</th>
                            <th>Nama Pasien</th>
                            <th>Diagnosa</th>
                            <th>Set Nota Apotek</th>
                            <th>Status Pemeriksaan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($detailkunjungans as $detailkunjungan)
                            <tr>
                                <td>{{ $detailkunjungan->id }}</td>
                                <td>{{ $detailkunjungan->kunjungan->pasien->nama }}</td>
                                <td>{{ $detailkunjungan->diagnosa }}</td>
                                <td>
                                    @if($detailkunjungan->resep == null || $detailkunjungan->apotek_id !== null)
                                    <button class="btn btn-sm btn-primary" disabled="disabled"><i class="fa-solid fa-notes-medical"></i></button>
                                    @else 
                                    <a href="{{ route('pharmacy.prescriptionView', $detailkunjungan->id) }}"
                                        class="btn btn-sm btn-primary">
                                        <i class="fa-solid fa-notes-medical"></i>
                                    </a>
                                    @endif
                                </td>
                                <td>
                                    @if ($detailkunjungan->apotek_id == null)
                                        <p class="badge bg-danger">
                                            not complete
                                        </p>
                                    @else
                                        <p class="badge bg-success">
                                            complete data
                                        </p>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>
    <script src="{{ asset('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )
    </script>

    <script src="assets/js/main.js"></script>
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    @if (session()->has('success'))
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Success',
                text: "{{ session('success') }}",
                showConfirmButton: true,
                timer: 2000
            });
        </script>
    @endif
@endpush
