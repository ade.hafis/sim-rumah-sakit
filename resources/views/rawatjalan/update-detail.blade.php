@extends('templates.master')
@section('title', 'Rawat Jalan / Update')
@section('page-name', 'Update Result Kunjungan')
@push('styles')
@endpush
@section('content')
<section id="basic-vertical-layouts">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="container">
                    <div class="card-content">
                        <div class="card-body">
                            <form action="{{ route('rawatjalan.update', $detailkunjungan->id) }}" method="post"
                                class="form form-vertical">
                                @csrf
                                @method('PATCH')
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group mb-3">

                                                @if($detailkunjungan->user->roles == 'dokter')
                                                <h6 class="card-title"
                                                    style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                    Dokter</h6>
                                                <select class="form-select" name="user_id">
                                                    @foreach ($dokters as $dokter)
                                                    <option value="{{ $dokter->id }}" {{ ($detailkunjungan->user_id ==
                                                        $dokter->id)? 'selected' : '' }}>
                                                        000{{ $dokter->id }} {{ $dokter->name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                @else
                                                <h6 class="card-title"
                                                    style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                    Perawat</h6>
                                                <select class="form-select" name="user_id">
                                                    @foreach ($dokterse as $dokter)
                                                    <option value="{{ $dokter->id }}" {{ ($detailkunjungan->user_id ==
                                                        $dokter->id)? 'selected' : '' }}>
                                                        000{{ $dokter->id }} {{ $dokter->name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                @endif
                                            </div>
                                        </div>
                                        @if($detailkunjungan->diagnosa == null && $detailkunjungan->resep == null)
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="hidden" name="diagnosa" value="" id="contact-info-vertical"
                                                    class="form-control" placeholder="Diagnosa">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <input type="hidden" name="resep" id="password-vertical" value=""
                                                    class="form-control" placeholder="Resep">
                                            </div>
                                        </div>
                                        @else
                                        <div class="col-12">
                                            <div class="form-group mb-3">
                                                <h6 class="card-title"
                                                    style="font-size: 18px; font-family: 'Montserrat', sans-serif;">
                                                    Diagnosa</h6>
                                                <input type="text" name="diagnosa"
                                                    value="{{ $detailkunjungan->diagnosa }}" id="contact-info-vertical"
                                                    class="form-control" name="contact" placeholder="Diagnosa">
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Resep
                                            </h6>
                                            <input type="text" name="resep" id="password-vertical"
                                                value="{{ $detailkunjungan->resep }}" class="form-control"
                                                placeholder="Resep">
                                        </div>
                                        @endif
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Poli
                                            </h6>
                                            <select class="form-select" name="poli_id">
                                                @foreach ($polis as $poli)
                                                <option value="{{ $poli->id }}" {{ ($detailkunjungan->poli_id ==
                                                    $poli->id)
                                                    ? 'selected' : '' }}>
                                                    {{ $poli->name_poli }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        @if($detailkunjungan->room_id == null)
                                        <fieldset class="form-group">
                                            <input type="hidden" name="room_id" value="">
                                        </fieldset>
                                        @else
                                        <div class="form-group mb-3">
                                            <h6 class="card-title"
                                                style="font-size: 18px; font-family: 'Montserrat', sans-serif;">Room
                                            </h6>
                                            <select class="form-select" name="room_id">
                                                @foreach ($rooms as $room)
                                                <option value="{{ $room->id }}" {{ ($detailkunjungan->room_id ==
                                                    $room->id)
                                                    ? 'selected' : '' }}>
                                                    {{ $room->name_room }}</option>
                                                @endforeach
                                            </select>
                                            @endif

                                            <br><br>
                                            <p style="font-size: 14px; margin-top: 7px; font-weight: bold; color: red;">
                                                <i>*perbarui data yang dibutuhkan</i>
                                            </p>
                                            <div class="col-12 d-flex justify-content-end">
                                                <a href="/rawat-jalan/detail-kunjungan"
                                                    class="btn btn-light-secondary me-3 mb-1 mt-4">Back</a>
                                                <button type="submit" class="btn btn-primary me-3 mb-1 mt-4">
                                                    <i class="fa-regular fa-floppy-disk">
                                                    </i> Simpan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
@endpush