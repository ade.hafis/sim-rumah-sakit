@extends('templates.master')
@section('title', 'Detail Data Perawatan')
@section('page-name', 'Detail Data Perawatan')
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')

<section class="section">
        <div class="card">
            <div class="container ">
                <div class="card-header">
                <select class="form-select w-25" id="sortingSelect">
                    <option value="{{ route('rawatjalan.index') }}" {{ request()->is('rawat-jalan/detail-kunjungan')
                        ? 'selected disabled' : '' }}>Semua Data Kunjungan</option>
                    <option value="{{ route('rawatjalan.sudahPemeriksaan') }}" {{ request()->
                        is('rawat-jalan/detail-kunjungan/sudah-pemeriksaan') ? 'selected disabled' : '' }}>Sudah
                        Pemeriksaan</option>
                    <option value="{{ route('rawatjalan.belumPemeriksaan') }}" {{ request()->
                        is('rawat-jalan/detail-kunjungan/belum-pemeriksaan') ? 'selected disabled' : '' }}>Belum
                        Pemeriksaan</option>
                </select>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="table1">
                <thead>
                    <tr>
                        <th>ID Kunjungan</th>
                        <th>Nama Pasien</th>
                        <th>Jadwal</th>
                        <th>Room</th>
                        <th>Penanganan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detailkunjungans as $detailkunjungan)
                    <tr>
                        <td>{{ $detailkunjungan->id }}</td>
                        <td>{{ $detailkunjungan->kunjungan->pasien->nama }}</td>
                        <td>{{ date('d F Y', strtotime($detailkunjungan->kunjungan->tanggal_kunjungan)) }}</td>

                        @if($detailkunjungan->room)
                        <td>{{ $detailkunjungan->room->name_room }}</td>
                        @else
                        <td> - </td>
                        @endif
                        
                        @if($detailkunjungan->user)
                        <td>{{ $detailkunjungan->user->roles }} - {{ $detailkunjungan->user->name }}</td>
                        @else
                        <td> - </td>
                        @endif

                        <td>
                        <form action="{{ route('rawatjalan.destroy', $detailkunjungan) }}" method="post">
                            <button type="button" class="btn btn-sm btn-primary block" data-bs-toggle="modal"
                                data-bs-target="#exampleModalCenter{{ $detailkunjungan->id }}">
                                <i class="fa-solid fa-eye"></i>
                            </button>
                            <div class="modal fade" id="exampleModalCenter{{ $detailkunjungan->id }}" tabindex="-1"
                                role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="padding: 20px;">
                                            <h2 class="modal-title" id="exampleModalCenterTitle">Detail Result Kunjungan
                                            </h2>
                                            <button type="button" class="close" data-bs-dismiss="modal"
                                                aria-label="Close">
                                                <i data-feather="x"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body" style="max-height: 450px; overflow-y: auto;">
                                            <h6 style="font-size: 16px;">Data Jadwal Pasien</h6>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p style="font-size: 14px;">Nama Pasien</p>
                                                </div>
                                                <div class="col-7">
                                                    <p style="font-size: 14px;">{{
                                                        $detailkunjungan->kunjungan->pasien->nama }}</p>
                                                </div>
                                                <div class="col-5">
                                                    <p style="font-size: 14px;">Tanggal Kunjungan</p>
                                                </div>
                                                <div class="col-7">
                                                    <p style="font-size: 14px;">{{ date('d F Y',
                                                        strtotime($detailkunjungan->kunjungan->tanggal_kunjungan)) }}
                                                    </p>
                                                </div>

                                                @if($detailkunjungan->user)
                                                    @if($detailkunjungan->user->roles == 'dokter')
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Dokter</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $detailkunjungan->user->name }}</p>
                                                    </div>
                                                    @elseif($detailkunjungan->user->roles == 'perawat')
                                                    <div class="col-5">
                                                        <p style="font-size: 14px;">Perawat</p>
                                                    </div>
                                                    <div class="col-7">
                                                        <p style="font-size: 14px;">{{ $detailkunjungan->user->name }}</p>
                                                    </div>
                                                    @endif
                                                @else 

                                                <div class="col-5">
                                                    <p style="font-size: 14px;">Penanganan</p>
                                                </div>
                                                <div class="col-7">
                                                    <p style="font-size: 14px;"></p>
                                                </div>
                                                @endif


                                                <div class="col-5">
                                                    <p style="font-size: 14px;">Room</p>
                                                </div>
                                                <div class="col-7">
                                                    @if($detailkunjungan->room)
                                                    <p style="font-size: 14px;">{{ $detailkunjungan->room->name_room }}
                                                    </p>
                                                    @else
                                                    <p></p>
                                                    @endif
                                                </div>

                                                <div class="col-5">
                                                    <p style="font-size: 14px;">Poli</p>
                                                </div>
                                                <div class="col-7">
                                                    @if($detailkunjungan->poli)
                                                    <p style="font-size: 14px;">{{ $detailkunjungan->poli->name_poli }}
                                                    </p>
                                                    @else
                                                    <p></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <h6 style="font-size: 16px; margin-top: 7px;">Result Pemeriksaan</h6>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p style="font-size: 14px;">Diagnosa</p>
                                                </div>
                                                <div class="col-7">
                                                    <p> {{ $detailkunjungan->diagnosa}} </p>
                                                </div>
                                                <div class="col-5">
                                                    <p style="font-size: 14px; margin-top: 7px;">Resep</p>
                                                </div>
                                                <div class="col-7">
                                                    <p> {{ $detailkunjungan->resep}} </p>
                                                </div>
                                                {{-- <div class="col-5">
                                                    <p style="font-size: 14px; margin-top: 7px;">Hasil Lab</p>
                                                </div>
                                                <div class="col-7">
                                                    @if ($detailkunjungan->lab_id)
                                                    <a href="#" style="width: 70px; height: 30px;"
                                                        class="btn btn-sm btn-primary">Lihat</a>
                                                    @else
                                                    <p style="font-size: 14px; margin-top: 7px;"><i>belum mendapat hasil
                                                            lab</i></p>
                                                    @endif
                                                </div> --}}
                                                <div class="col-5">
                                                    <p style="font-size: 14px; margin-top: 7px;">Nota Apotek</p>
                                                </div>
                                                <div class="col-7">
                                                    @if ($detailkunjungan->apotek_id)
                                                    {{-- nota apotek href kelompok apotek --}}
                                                    <a href="#" style="width: 70px; height: 30px;"
                                                        class="btn btn-sm btn-primary">Lihat</a>
                                                    @else
                                                    <p style="font-size: 14px; margin-top: 7px;"><i>belum mendapat nota
                                                            apotek</i></p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer" style="padding: 10px;">
                                            <button type="button" class="btn btn-light-secondary"
                                                data-bs-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Close</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(empty($detailkunjungan->poli) && empty($detailkunjungan->dokter) ||
                            empty($detailkunjungan->diagnosa) && !empty($detailkunjungan->resep))
                            <button type="button" class="btn btn-sm btn-secondary block"
                                href="/rawat-jalan/update-detail" disabled>
                                <i class="fa-solid fa-pencil"></i>
                            </button>
                            @else
                            <a href="{{ route('rawatjalan.edit', $detailkunjungan->id) }}"
                                class="btn btn-sm btn-warning">
                                <i class="fa-solid fa-pencil"></i>
                            </a>
                            @endif

                            @if(empty($detailkunjungan->poli) && empty($detailkunjungan->dokter) &&
                            empty($detailkunjungan->diagnosa) && empty($detailkunjungan->resep) ||
                            empty($detailkunjungan->room_id))
                            <a class="btn btn-sm btn-info block"
                                href="{{ route('rawatjalan.show', $detailkunjungan) }}">
                                <i class="fa-solid fa-briefcase-medical"></i>
                            </a>
                            @else
                            <button type="button" class="btn btn-sm btn-secondary block" href="" disabled>
                                <i class="fa-solid fa-briefcase-medical"></i>
                            </button>
                            @endif

                            @if(!empty($detailkunjungan->poli) && empty($detailkunjungan->dokter) &&
                            (empty($detailkunjungan->diagnosa) || empty($detailkunjungan->resep)))
                            <a class="btn btn-success btn-sm"
                                href="{{ route('rawatjalan.adddiagnosa', $detailkunjungan) }}">
                                <i class="fa-solid fa-paste"></i>
                            </a>
                            @else
                            <button type="button" class="btn btn-sm btn-secondary" disabled>
                                <i class="fa-solid fa-paste"></i>
                            </button>
                            @endif

                            
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </form>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>
</div>


<script>
    document.getElementById('sortingSelect').addEventListener('change', function() {
        var selectedOption = this.options[this.selectedIndex];
        var route = selectedOption.value;

        if (route) {
            window.location.href = route;
        }
    });
</script>

<script src="{{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
<script>
    let dataTable = new simpleDatatables.DataTable(
                    document.getElementById("table1")
                )               
</script>

<script src="assets/js/main.js"></script>

@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(session()->has('success'))
<script>
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text : "{{ session('success') }}",
    showConfirmButton: true,
    timer: 2000
    });
</script>
@endif
@endpush