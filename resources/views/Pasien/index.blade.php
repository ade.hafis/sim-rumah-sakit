@extends('templates.master')
@section('title', 'Master Pasien')
@section('page-name', 'Master Pasien')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
@endpush
@section('content')
    <div class="page-heading">
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">
                        Data Pasien
                    </h5>
                </div>
                <div class="card-body">
                    <a href="{{ route('pasien.create') }}" class="btn btn-primary"><i class="bi bi-plus"></i>
                        Data</a>
                    <table class="table table-striped" id="table1">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pasien</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>No Hp</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($pasien as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->jenis_kelamin }}</td>
                                    <td>{{ date('d-m-Y', strtotime($item->tanggal_lahir)) }}</td>
                                    <td>{{ $item->no_hp }}</td>
                                    <td>
                                        <a href="{{ route('pasien.edit', $item->id) }}" class="btn btn-warning text-white"><i
                                                class="bi bi-pencil-square"></i></a>
                                       <button type="button" onclick="deleteData({{ $item->id }})" class="btn btn-danger"><i class="bi bi-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>
@endsection
@push('scripts')
    <script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("table1")
        )

        function deleteData(id) {
            swal({
                title: "Apakah anda yakin?",
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if(willDelete) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        url: "{{ route('pasien.delete',':id') }}".replace(':id', id),
                        type: "DELETE",
                        success: function(response) {
                            swal({
                                title: "Berhasil!",
                                text: "Data berhasil dihapus!",
                                icon: "success",
                                button: "OK",
                            }).then((value) => {
                                location.reload();
                            });
                        },
                        error: function(response) {
                            swal({
                                title: "Gagal!",
                                text: "Data gagal dihapus!",
                                icon: "error",
                                button: "OK",
                            });
                        }
                    });
                }
            });
        }
    </script>
@endpush
