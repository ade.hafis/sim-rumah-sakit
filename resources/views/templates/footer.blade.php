<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>
                <script type="text/javascript">
                    document.write(new Date().getFullYear());
                </script> &copy; DECANT
            </p>
        </div>
        <div class="float-end">
            <p>Crafted with <span class="text-danger"><i class="bi bi-heart"></i></span> by <a href="https://www.instagram.com/decant_/" target="_blank">D3 IT A 2022</a></p>
        </div>
    </div>
</footer>