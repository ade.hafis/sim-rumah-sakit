@extends('templates.master')
@section('title', 'Tambah Data Pasien')
@section('page-name', 'Tambah Data Pasien')
@push('styles')
@endpush
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ route('resultLab.store') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-2">
                            <label for="">Nama Pasien : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <select class="form-select" id="basicSelect" name="kunjungan_id">
                                    <option value="">Select pasien</option>
                                    @foreach ($kunjungans as $kunjungan)
                                        <option value="{{ $kunjungan->id }}">(Kunjungan-{{ $kunjungan->id }}) -
                                            {{ $kunjungan->pasien->nama }}
                                        </option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Lab : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <select class="form-select" id="basicSelect" name="lab_id">
                                    <option value="">Select LAB</option>
                                    @foreach ($labs as $lab)
                                        <option value="{{ $lab->id }}">{{ $lab->name }}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Dokter : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <select class="form-select" id="basicSelect" name="user_id">
                                    <option value="">Select Dokter</option>
                                    @foreach ($dokters as $doctor)
                                        <option value="{{ $doctor->id }}">{{ $doctor->name }} ( {{ $doctor->no_pegawai }}
                                            )
                                        </option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
