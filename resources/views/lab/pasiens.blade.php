@extends('templates.master')
@section('title', 'Data Pasien')
@section('page-name', 'Data Pasien')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')
    <div class="mb-2">
        <a href={{ route('resultLab.create') }} class="btn btn-primary"><i class="bi bi-plus"></i>Tambah Data Pasien</a>
    </div>
    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success m-3 ">
                            {{ session('success') }}
                        </div>
                    @endif
                    <table class="table" id="tablePasien">
                        <thead>
                            <tr>
                                <th class="text-center">Result ID</th>
                                <th class="text-center">Nama Pasien</th>
                                <th class="text-center">Nama Lab</th>
                                <th class="text-center">Dokter</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Hasil Lab</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($resultLabs as $result)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="text-center">{{ $result->kunjungan->pasien->nama }}</td>
                                    <td class="text-center">{{ $result->lab->name }}</td>
                                    <td class="text-center">{{ $result->user->name }}</td>
                                    <td class="text-center">
                                        @if ($result->status === 'success')
                                            <span class="badge bg-success">Success</span>
                                        @elseif ($result->status === 'proses' || $result->status == null)
                                            <span class="badge bg-warning">proses</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($result->status === 'success')
                                            <a href={{ route('resultLab.download', $result->id) }} class="btn btn-light">
                                                <i class="bi bi-download"></i>
                                            </a>
                                        @elseif ($result->status === 'proses' || $result->status == null)
                                            <button class="btn btn-light" disabled>
                                                <i class="bi bi-x-circle"></i>
                                            </button>
                                        @endif

                                    </td>
                                    <td class="text-center">{{ $result->created_at->format('d M Y') }}</td>
                                    <td>
                                        <div class="d-flex justify-content-center gap-2">
                                            <a href={{ route('resultLab.edit', $result->id) }}
                                                class="btn btn-info rounded text-white" style="outline: none">
                                                <i class="bi bi-pencil-square"></i>
                                            </a>
                                            <button class="btn btn-danger rounded " style="outline: none"
                                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                <i class="bi bi-trash"></i>
                                            </button>
                                            <a href={{ route('resultLab.show', $result->id) }}
                                                class="btn btn-primary rounded text-white" style="outline: none">
                                                <i class="bi bi-eye-fill"></i>
                                            </a>
                                            <div class="modal fade" id="exampleModal" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Hapus Data
                                                                Lab
                                                            </h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah anda yakin ingin menghapus data ini?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Cancel</button>
                                                            <form action={{ route('resultLab.destroy', $result->id) }}
                                                                method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit"
                                                                    class="btn btn-danger">Delete</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="8">Belum ada data</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <script src="{{ asset('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("tablePasien")
        )
    </script>

    <script src="assets/js/main.js"></script>
@endsection
@push('scripts')
@endpush
