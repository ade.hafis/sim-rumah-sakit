@extends('templates.master')
@section('title', 'Tambah Data Lab')
@section('page-name', 'Tambah Data Lab')
@push('styles')
@endpush
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('lab.store') }}" method="post">
                        @csrf
                        <div class="col-2 mb-2">
                            <label for="id">Id Lab : </label>
                        </div>
                        <div class="col-10 mb-2">
                            <input type="text" class="form-control" id="id" name="code_lab"
                                placeholder="Masukkan ID Lab" value="{{ old('code_lab') }}" required>
                        </div>
                        <div class="col-2 mb-2">
                            <label for="name">Nama Lab : </label>
                        </div>
                        <div class="col-10 mb-2">
                            <input type="text" class="form-control" id="name" name="name"
                                placeholder="Masukkan Nama Lab" value="{{ old('name') }}" required>
                        </div>
                        <div class="col-2 mb-2">
                            <label for="description">Deskripsi : </label>
                        </div>
                        <div class="col-10 mb-2">
                            <textarea class="form-control" id="description" name="description" rows="3" required>{{ old('description') }}</textarea>
                        </div>
                        <div class="col-2 mb-2">
                            <label for="price">Harga : </label>
                        </div>
                        <div class="col-10 mb-2">
                            <input type="text" class="form-control" name="price" id="basicInput"
                                placeholder="Masukkan Biaya" value="{{ old('price') }}" required>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary" name="submit">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
