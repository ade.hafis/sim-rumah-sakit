@extends('templates.master')
@section('title', 'Data Lab')
@section('page-name', 'Data Lab')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')
    <div class="mb-3">
        <a href="{{ route('lab.create') }}" class="btn btn-primary"><i class="bi bi-plus"></i>Tambah Data Lab</a>
    </div>
    <div class="page-heading">
        <section class="section">
            <div class="card">
                @if (session('success'))
                    <div class="alert alert-success m-3 ">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="card-body">
                    <table class="table" id="tableLab">
                        <thead>
                            <tr>
                                <th class="text-center">ID Lab</th>
                                <th class="text-center">Nama Lab</th>
                                <th class="text-center">Deskripsi</th>
                                <th class="text-center">Biaya</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($labs as $lab)
                                <tr>
                                    <td class="text-center">{{ $lab->code_lab }}</td>
                                    <td class="text-center">{{ $lab->name }}</td>
                                    <td>{{ $lab->description }}</td>
                                    <td class="text-center">{{ $lab->price }}</td>
                                    <td>
                                        <div class="d-flex justify-content-center gap-2">
                                            <a href="/lab/update">
                                                <a href="{{ route('lab.edit', $lab->id) }}"
                                                    class="btn btn-info rounded text-white" style="outline: none">
                                                    <i class="bi bi-eye-fill"></i>
                                                </a>
                                            </a>
                                            <button class="btn btn-danger rounded text-white" style="outline: none"
                                                data-bs-toggle="modal" data-bs-target="#exampleModal">
                                                <i class="bi bi-trash"></i>
                                            </button>
                                            <div class="modal fade" id="exampleModal" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Hapus Data Lab
                                                            </h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                                aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah anda yakin ingin menghapus data ini?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Cancel</button>
                                                            <form action="{{ route('lab.destroy', $lab->id) }}"
                                                                method="post">
                                                                @method('delete')
                                                                @csrf
                                                                <button type="submit"
                                                                    class="btn btn-danger">Delete</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">
                                        <p class="text-center my-5">Belum ada lab yang terdaftar</p>
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
    <script src="{{ asset('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("tableLab")
        )
    </script>

    <script src="assets/js/main.js"></script>
@endsection
@push('scripts')
@endpush
