@extends('templates.master')
@section('title', 'Edit Data Pasien')
@section('page-name', 'Edit Data Pasien')
@push('styles')
@endpush
@section('content')
    <section class="section">
        <div class="card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action={{ route('resultLab.update', $resultLab->id) }} enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <div class="row">
                        <div class="col-2">
                            <label for="">Nama Pasien : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                {{-- {{ dd($resultLab->kunjungan->pasien) }} --}}
                                <select class="form-select" id="basicSelect" name="kunjungan_id">
                                    <option value={{ $resultLab->kunjungan_id }}>
                                        (Kunjungan-{{ $resultLab->kunjungan_id }}){{ $resultLab->kunjungan->pasien->nama }}
                                    </option>
                                    @foreach ($kunjungans as $kunjungan)
                                        @if ($kunjungan->id != $resultLab->kunjungan->id)
                                            <option value={{ $kunjungan->id }}> (Kunjungan-{{ $kunjungan->id }})
                                                {{ $kunjungan->pasien->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Lab : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <select class="form-select" id="basicSelect" name="lab_id">
                                    <option value={{ $resultLab->lab->id }}>{{ $resultLab->lab->name }}</option>
                                    @foreach ($labs as $lab)
                                        @if ($lab->id !== $resultLab->lab->id)
                                            <option value={{ $lab->id }}>{{ $lab->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Dokter : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <select class="form-select" id="basicSelect" name="user_id">
                                    <option value={{ $resultLab->user->id }}>{{ $resultLab->user->name }}</option>
                                    @foreach ($dokters as $dokter)
                                        @if ($dokter->id != $resultLab->user->id)
                                            <option value={{ $dokter->id }}>{{ $dokter->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Status : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <select class="form-select" id="basicSelect" name="status">
                                    <option value="proses">Proses</option>
                                    <option value="success">Success</option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Hasil : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <input class="form-control" type="file" name="hasil_lab">
                            </fieldset>
                        </div>
                        <div class="col-2">
                            <label for="">Deskripsi : </label>
                        </div>
                        <div class="col-10">
                            <fieldset class="form-group">
                                <textarea style="resize: none" class="form-control" name="description" id="" cols="30" rows="10">{{ $resultLab->description }}</textarea>
                            </fieldset>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
@endpush
