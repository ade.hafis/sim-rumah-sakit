@extends('templates.master')
@section('title', 'Apotek / Resep')
@section('page-name', 'Apotek')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')
    <div class="mb-2">
        <a href="{{ route('pharmacy.prescriptionView', $visitId) }}" class="btn btn-primary "><i
                class="bi bi-arrow-left"></i>Back</a>
    </div>
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h3 class="card-title">Resep</h3>
            <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#openModal" data-modalLevel="add"><i
                    class="bi bi-plus-circle-fill"></i> Add New Item</button>
        </div>
        <div class="card-body">
            <div class="">
            </div>
            <div class="">
                <div class="table-responsive rounded border">
                    <table class="table table-xs" id="resep">
                        <thead class="">
                            <tr class="">
                                <th class="fw-bold px-4">Medicine Name</th>
                                <th class="fw-bold px-4 text-center">Quantity</th>
                                <th class="fw-bold px-4 text-center">Price</th>
                                <th class="fw-bold px-4 text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($prescriptionDetails as $prescriptionDetail)
                                <tr class="">
                                    <td class="px-4">{{ $prescriptionDetail->drug()->name }}</td>
                                    <td class="px-4 text-center">{{ $prescriptionDetail->quantity }}</td>
                                    <td class="px-4 text-center">
                                        {{ $prescriptionDetail->quantity * $prescriptionDetail->drug()->price }}</td>
                                    <td class="px-4 text-center">
                                        <div class="btn-group" role="group">
                                            <button
                                                class="m-1 rounded bg-primary p-1 btn px-2 text-center fw-bolder text-white "data-bs-toggle="modal"
                                                data-bs-target="#prescriptionDetails{{ $prescriptionDetail->id }}"
                                                data-modalLevel="update">
                                                <i class="bi bi-pencil"></i>
                                            </button>
                                            <form
                                                action="{{ route('pharmacy.destroyDrugInPrescription', $prescriptionDetail->id) }}"
                                                method="post">
                                                @csrf
                                                <input type="hidden" name="drugId"
                                                    value="{{ $prescriptionDetail->drug_id }}">
                                                <input type="hidden" name="quantity"
                                                    value="{{ $prescriptionDetail->quantity }}">
                                                <button type="submit"
                                                    href="{{ route('pharmacy.destroyDrugInPrescription', $prescriptionDetail->id) }}"
                                                    class="m-1 rounded bg-danger p-1 btn px-2 text-center fw-bolder text-white">
                                                    <i class="bi bi-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    {{-- Modal --}}
    <div class="modal modal-borderless fade" id="openModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Add New Medicine Item</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {{-- Form --}}
                    <form class="form" id="receipt-form" method="POST"
                        action="{{ route('pharmacy.createDrugInPrescription') }}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" name="prescriptionId" value="{{ $prescriptionId }}">
                                <div class="form-group">
                                    <label for="medicine-name-column">Medicine Name</label>
                                    <select id="medicine-name-column" class="form-control form-select"
                                        placeholder="Select Medicine Item" name="medicineId" required>
                                        <option selected disabled hidden>---- Select Drug ----</option>
                                        @foreach ($drugs as $drug)
                                            <option value="{{ $drug->id }}">{{ $drug->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="quantity-column">Quantity</label>
                                    <input type="number" min="0" id="quantity-column" class="form-control"
                                        value='' placeholder="Quantity" name="quantity" value="" required>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary me-1 mb-1" onclick="resetForm()">Reset</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @foreach ($prescriptionDetails as $prescriptionDetail)
        <div class="modal modal-borderless fade" id="prescriptionDetails{{ $prescriptionDetail->id }}" tabindex="-1"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Medicine quantity</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        {{-- Form --}}
                        <form class="form" id="receipt-form" method="POST"
                            action="{{ route('pharmacy.updateDrugInPrescription') }}">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <input type="hidden" name="prescriptionDetailId"
                                        value="{{ $prescriptionDetail->id }}">

                                    <input type="hidden" name="currentQuantity"
                                        value="{{ $prescriptionDetail->quantity }}">

                                    <input type="hidden" name="drugId" value="{{ $prescriptionDetail->drug_id }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="quantity-column">Quantity</label>
                                        <input type="number" min="0" id="quantity-column" class="form-control"
                                            placeholder="Quantity" name="quantity"
                                            value={{ $prescriptionDetail->quantity }}>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    </div>

@endsection
@push('scripts')
    <script src="{{ asset('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("resep")
        )
    </script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const openModal = document.getElementById("openModal");
            const modalTitleElement = document.getElementById("exampleModalLabel");

            openModal.addEventListener("show.bs.modal", function(event) {
                const modalTriggerButton = event.relatedTarget;
                const modalLevel = modalTriggerButton.getAttribute("data-modalLevel");
                // console.log("modalLevel:", modalLevel);
                const modalTitle = modalLevel === 'add' ? 'Add Medicine Item' : 'Update Medicine Item';
                modalTitleElement.innerText = modalTitle;

            });
        });

        function resetForm() {
            document.getElementById("receipt-form").reset();
        }
    </script>
@endpush
