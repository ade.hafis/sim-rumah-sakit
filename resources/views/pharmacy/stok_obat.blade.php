@extends('templates.master')
@section('title', 'Apotek / Stok Obat')
@section('page-name', 'Apotek')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/extensions/simple-datatables/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/scss/pages/simple-datatables.scss') }}">
@endpush
@section('content')
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h3 class="card-title">Stok Obat</h3>
            <div class="">
                <a class="btn btn-info" href="{{ route('pharmacy.downloadDrugExcel') }}"><i
                        class="bi bi-file-earmark-arrow-down"></i>
                    download excel</a>
                <button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#openModal" data-modalLevel="add"><i
                        class="bi bi-plus-circle-fill"></i> Add New Item</button>
            </div>
        </div>
        <div class="card-body">
            <div class="">
            </div>
            <div class="">
                <div class="table-responsive rounded border">
                    <table class="table table-xs" id="resep">
                        <thead class="">
                            <tr class="">
                                <th class="fw-bold px-4">Medicine Name</th>
                                <th class="fw-bold px-4">Medicine ID</th>
                                <th class="fw-bold px-4 text-center">Stock</th>
                                <th class="fw-bold px-4 text-center">Price</th>
                                <th class="fw-bold px-4 text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($drugs as $drug)
                                <tr class="">
                                    <td class="px-4"> {{ $drug->name }}</td>
                                    <td class="px-4">{{ $drug->name_code }}</td>
                                    <td class="px-4 text-center">{{ $drug->stock }}</td>
                                    <td class="px-4 text-center">Rp. {{ $drug->price }}</td>
                                    <td class="px-4 text-center">
                                        <div class="btn-group" role="group">
                                            <button
                                                class="m-1 rounded bg-primary p-1 btn px-2 text-center fw-bolder text-white"
                                                data-bs-toggle="modal" data-bs-target="#update{{ $drug->id }}"
                                                data-modalLevel="add">
                                                <i class="bi bi-pencil"></i>
                                            </button>
                                            <form action="{{ route('pharmacy.destroyMedicine', $drug->id) }}"
                                                method="post">
                                                @csrf
                                                <button type="submit"
                                                    class="m-1 rounded bg-danger p-1 btn px-2 text-center fw-bolder text-white">
                                                    <i class="bi bi-trash"></i>
                                                </button>
                                            </form>
                                            <a href="{{ route('pharmacy.detailMedicine', $drug->id) }}"
                                                class="m-1 rounded bg-success p-1 btn px-2 text-center fw-bolder text-white">
                                                <i class="bi bi-eye"></i>
                                            </a>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    {{-- Modal --}}
    <div class="modal modal-borderless fade" id="openModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Add New Medicine Item</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {{-- Form --}}
                    <form class="form" id="medicine-form" method="POST" action="{{ route('pharmacy.createMedicine') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="medicine-name-column">Medicine Name</label>
                                    <input type="text" id="medicine-name-column" class="form-control" value=''
                                        placeholder="Medicine Name" name="name" required>
                                    <label for="stock-column">Stock</label>
                                    <input type="number" min="0" id="stock-column" class="form-control"
                                        value='' placeholder="Stock" name="stock" required>
                                    <label for="price-column">Price</label>
                                    <div class="input-group">
                                        <span class="input-group-text" id="basic-addon1">Rp</span>
                                        <input type="text" id="price-column" class="form-control" value=''
                                            name="price" placeholder="Price" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="guide-column">How to use</label>
                                    <textarea rows="4" cols="50" id="guide-column" class="form-control" value=''
                                        placeholder="How to use" name="guide" required></textarea>
                                    <label for="side-effect-floating">Side Effect</label>
                                    <textarea rows="4" cols="50" id="side-effect-floating" class="form-control" value=''
                                        name="sideEffect" placeholder="Side Effect" required></textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary me-1 mb-1" onclick="resetForm()">Reset</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    @foreach ($drugs as $drug)
        <div class="modal modal-borderless fade" id="update{{ $drug->id }}" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Update Medicine Item</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        {{-- Form --}}
                        <form class="form" id="medicine-form" method="POST"
                            action="{{ route('pharmacy.updateMedicine', $drug->id) }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="medicine-name-column">Medicine Name</label>
                                        <input type="text" id="medicine-name-column" class="form-control"
                                            value='{{ $drug->name }}' placeholder="Medicine Name" name="name"
                                            required>
                                        <label for="price-column">Price</label>
                                        <div class="input-group">
                                            <span class="input-group-text" id="basic-addon1">Rp</span>
                                            <input type="text" id="price-column" class="form-control"
                                                value='{{ $drug->price }}' name="price" placeholder="Price" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="guide-column">How to use</label>
                                        <textarea rows="4" cols="50" id="guide-column" class="form-control" placeholder="How to use"
                                            name="guide" required>{{ $drug->how_to_use }}</textarea>
                                        <label for="side-effect-floating">Side Effect</label>
                                        <textarea rows="4" cols="50" id="side-effect-floating" class="form-control" value='{{ $drug->effect }}'
                                            name="sideEffect" placeholder="Side Effect" required>{{ $drug->side_effect }}</textarea>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-secondary me-1 mb-1"
                            onclick="resetForm()">Reset</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach

@endsection
@push('scripts')
    <script src="{{ asset('assets/extensions/simple-datatables/umd/simple-datatables.js') }}"></script>
    <script>
        let dataTable = new simpleDatatables.DataTable(
            document.getElementById("resep")
        )
    </script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            const openModal = document.getElementById("openModal");
            const modalTitleElement = document.getElementById("exampleModalLabel");

            openModal.addEventListener("show.bs.modal", function(event) {
                const modalTriggerButton = event.relatedTarget;
                const modalLevel = modalTriggerButton.getAttribute("data-modalLevel");
                // console.log("modalLevel:", modalLevel);
                const modalTitle = modalLevel === 'add' ? 'Add New Medicine Item' : 'Update Medicine Item';
                modalTitleElement.innerText = modalTitle;

            });
        });

        function resetForm() {
            document.getElementById("medicine-form").reset();
        }
    </script>
@endpush
