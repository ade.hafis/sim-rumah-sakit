<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CETAK MIN</title>
    <link rel="stylesheet" href="{{ asset('assets/css/main/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main/app-dark.css') }}">
    @stack('styles')
    <style>
        @page {
        size: auto;
        size: TABLOID;
        margin: 0mm;
        }
    </style>
</head>
<body>
    
    
    
                                                @php
                                                    $no = 1;
                                                @endphp
                                                <div class="">
                                                <div class="d-flex justify-content-center align-items-center">
                                                    <div class="logo">
                                                        <a href="{{ route('dashboard') }}">
                                                            <img src="{{ asset('assets/images/logo.png') }}" alt="Logo" srcset="" class="img-fluid"
                                                                style="width: 70px; height:auto;">
                                                        </a>
                                                    </div>
                                                    <div class="fw-bold">
                                                         PT. PEMNS
                                                    </div>
                                                </div>
                                                
                                                <div class="" style="max-width: 100%;" id="">
                                                    <div class="card m-4">
                                                        <div class="card-content">
                                                            <div class="card-header">
                                                                <h5 class="card-title" id="Title">
                                                                    Reciept Pasien / {{ $invoice->dkunjungan->kunjungan->pasien->nama}}</h5>
                                                                    
                                                            </div>
                                                            <div class="card-body">
                                                            <div class="">
                                                                    <div class="card-flex justify-content-start m-4">
                                                                        
                                                                        
                                                                        <div>INVOICE/RSPEMNS/{{ strtotime($invoice->created_at )}}/1290{{$invoice->id}}</div>
                                                                    </div>
                                                                    <div class="table-responsive rounded border m-4">
                                                                        <table
                                                                            class="table table-xs justify-content-start ">
                                                                            <thead class="">
                                                                                <tr class="">
                                                                                    <th class="fw-bold px-4">Order</th>
                                                                                    <th class="fw-bold px-4 text-start">Keterangan
                                                                                        </th>
                                                                                        <th class="fw-bold px-4">Desc</th>
                                                                                        <th
                                                                                        class="fw-bold px-4 text-center">
                                                                                        @harga</th>
                                                                                        <th
                                                                                        class="fw-bold px-4 text-center">
                                                                                        Qty</th>
                                                                                    <th
                                                                                        class="fw-bold px-4 text-center">
                                                                                        @Total</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody class="fw-normal">
                                                                                @if(isset($invoice->dkunjungan->apotek_id))

                                                                                    <tr>
                                                                                        <th colspan="2"
                                                                                            class="p-2 fw-bolder ">
                                                                                            <div
                                                                                                style="padding: 0 0 0 80px; border-bottom: 0px;"
                                                                                                class="text-start">
                                                                                                Drugs
                                                                                            </div>
                                                                                        </th>
                                                                                    </tr>
                                                                                    
                                                                                    @foreach($apotek->where('prescription_id', $invoice->dkunjungan->invoiceResep->id) as $items)

                                                                                    @php
                                                                                        $dataObat = $drugs->where('id',$items->drug_id)->first();
                                                    
                                                                                    @endphp
                                                                                    <tr class="fw-normal">
                                                                                        
                                                                                        <th class="fw-normal text-center">{{$loop->iteration}}</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            {{ $dataObat->name}}
                                                                                        </th>
                                                                                        <th class="fw-normal">-
                                                                                        </th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($dataObat->price)
                                                                                        </th>
                                                                                        <th class="fw-normal">{{$items->quantity}}</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($dataObat->price * $items->quantity)
                                                                                        </th>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                @endif

                                                                                <tr>
                                                                                    <th colspan="2"
                                                                                        class="p-2 fw-bolder">
                                                                                        <div
                                                                                            style="padding: 0 0 0 80px;"
                                                                                            class="text-start">
                                                                                            Services
                                                                                        </div>
                                                                                    </th>
                                                                                </tr>

                                                                                <tr class="">
                                                                                    <th class="fw-normal text-center">1</th>
                                                                                    <th class="fw-normal px-4 text-start">
                                                                                        Biaya Registrasi Rumah Sakit
                                                                                    </th>
                                                                                    <th class="fw-normal">Rawat Jalan
                                                                                    </th>
                                                                                    <th class="fw-normal px-4 text-start">
                                                                                        Rp. 30.000,00
                                                                                    </th>
                                                                                    <th class="fw-normal">1</th>
                                                                                    <th class="fw-normal px-4 text-start">
                                                                                        Rp. 30.000,00
                                                                                    </th>
                                                                                </tr>
                                                                                @if($lab->where('kunjungan_id',$invoice->dkunjungan->kunjungan->id) !== null)
                                                                                    <tr>
                                                                                        <th colspan="2"
                                                                                            class="p-2 fw-bolder">
                                                                                            <div
                                                                                                style="padding: 0 0 0 80px;"
                                                                                                class="text-start">
                                                                                                Labs
                                                                                            </div>
                                                                                        </th>
                                                                                    </tr>
                                                                                    @foreach($lab->where('kunjungan_id',$invoice->dkunjungan->kunjungan->id) as $res)
                                                                                    <tr class="">
                                                                                        <th class="fw-normal text-center">{{$loop->iteration}}</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            {{$res->lab->name}}
                                                                                        </th>
                                                                                        <th class="fw-normal">
                                                                                           {{$res->description ?? '-'}}
                                                                                        </th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($res->lab->price)
                                                                                        </th>
                                                                                        <th class="fw-normal">1</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($res->lab->price)
                                                                                        </th>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                @endif
                                                                                </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="border mt-4 rounded m-4">
                                                                        <div class="d-flex m-1 justify-content-between p-2">
                                                                            <div class="text-start">
                                                                                <div style="font-size: 12px;">- PT.PEMNS NPWP : 213131000</div>
                                                                                <div style="font-size: 12px;">- JIKA TERDAPAT KESALAHAN DAPAT
                                                                                    MENGHUBUNGI KAMI</div>
                                                                                </div>
                                                                            <div class="text-start d-flex fw-bold" style="margin: 0  0 0 0;">
                                                                                <div class="mx-4">Total Biaya</div>
                                                                                <div class="mx-2">@money($invoice->dkunjungan->pembayaran)</div>    
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="border mt-4 rounded m-4">
                                                                        <div class="m-2">
                                                                            <p>Keluhan : {{$invoice->dkunjungan->kunjungan->keluhan}}</p>
                                                                            <p>diagnosa : {{$invoice->dkunjungan->diagnosa}}</p>
                                                                            <div>
                                                                                Resep Dokter :
                                                                                {{$invoice->dkunjungan->resep}}
                                                                            </div>
                                                                            <div>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-flex justify-content-end" style="height:120px; margin-top: 40px; margin-right: 100px;">
                                                                        <div class="text-center">

                                                                            <div>
                                                                                Admin Pembayaran
                                                                            </div>
                                                                            
                                                                            
                                                                            
                                                                            <div style="margin-top: 75px" class="text-center">
                                                                                {{Auth::user()->name}}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                           
                                                <script>
                                                    window.print()
                                                </script>
</body>
</html>