@extends('templates.master')
@section('title', 'Pembayaran')
@section('page-name', 'Pembayaran')
@push('styles')
    <link rel="stylesheet" href={{ asset('assets/extensions/simple-datatables/style.css') }}>
    <link rel="stylesheet" href={{ asset('assets/scss/pages/simple-datatables.scss') }}>
@endpush
@section('content')
<div class="row ">
        <div class="col">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-flex align-items-center place-content-between">
                                <div class="">
                                    <div class="bg-info rounded h-100 p-5 mx-3 d-flex justify-content-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="77" height="48" viewBox="0 0 77 48" fill="none">
                                        <path d="M0 3.5C0 1.567 1.567 0 3.5 0C5.433 0 7 1.567 7 3.5V44.5C7 46.433 5.433 48 3.5 48C1.567 48 0 46.433 0 44.5V3.5Z" fill="#ffffff"/>
                                        <rect x="31.278" y="18.5546" width="5.81905" height="33.9571" rx="2.90953" transform="rotate(41 31.278 18.5546)" fill="#ffffff"/>
                                        <rect x="71.8649" y="13.5785" width="5.81905" height="37.0396" rx="2.90953" transform="rotate(45 71.8649 13.5785)" fill="#ffffff"/>
                                        <rect x="76.3702" y="13.9135" width="4.73083" height="13.7485" rx="2.36542" transform="rotate(98 76.3702 13.9135)" fill="#ffffff"/>
                                        <rect x="71.803" y="13.7502" width="4.15421" height="13.143" rx="2.0771" transform="rotate(-3 71.803 13.7502)" fill="#ffffff"/>
                                        <rect x="27.9846" y="22.1301" width="5.81905" height="30.8265" rx="2.90953" transform="rotate(-45 27.9846 22.1301)" fill="#ffffff"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="flex-fill">
                                    <div>
                                        <h3>@money($totalIncome)</h3>
                                        <p class="text-bold fw-bold">TOTAL PEMASUKAN</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col">
        <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="d-flex align-items-center place-content-between">
                                <div class="">
                                    <div class="bg-danger rounded h-100 p-5 mx-3 d-flex justify-content-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="77" height="48" viewBox="0 0 77 48" fill="none">
                                        <path d="M0 3.5C0 1.567 1.567 0 3.5 0C5.433 0 7 1.567 7 3.5V44.5C7 46.433 5.433 48 3.5 48C1.567 48 0 46.433 0 44.5V3.5Z" fill="#ffffff  "/>
                                        <rect width="5.81905" height="33.9571" rx="2.90953" transform="matrix(0.75471 -0.656059 -0.656059 -0.75471 31.278 41.4454)" fill="#ffffff   "/>
                                        <rect width="5.81905" height="37.0396" rx="2.90953" transform="matrix(0.707107 -0.707107 -0.707107 -0.707107 71.8649 46.4215)" fill="#ffffff    "/>
                                        <rect width="4.73083" height="13.7485" rx="2.36542" transform="matrix(-0.139173 -0.990268 -0.990268 0.139173 76.3702 46.0865)" fill="#ffffff    "/>
                                        <rect width="4.15421" height="13.143" rx="2.0771" transform="matrix(0.99863 0.052336 0.052336 -0.99863 71.803 46.2498)" fill="#ffffff   "/>
                                        <rect width="5.81905" height="30.8265" rx="2.90953" transform="matrix(0.707107 0.707107 0.707107 -0.707107 27.9846 37.8699)" fill="#ffffff  "/>
                                    </svg>
                                    </div>
                                </div>
                                <div class="flex-fill">
                                    <div>
                                        <h3>@money($totalOutcome)</h3>
                                        <p class="text-bold fw-bold">TOTAL PENGELUARAN</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <section class="section">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Grafik Keuangan</h4>
            </div>
                <div class="card-body d-flex justify-content-center" id="keuanganChart">
                    
                </div>
            
        </div>
    </section>
    <section>
    <div class="card">
            <div class="card-header">
                <h5 class="card-title">
                    Akuntansi
                </h5>
            </div>
            <div class="card-body">
            <table class="table table-striped" id="akuntansi">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Arsip</th>
                            <th>Pengeluaran</th>
                            <th>Pemasukan</th>
                            <th>Keterangan</th>
                            
                        </tr>

                        </thead>
                        <tbody>
                            @foreach ($keuangan as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->tanggal_arsip }}</td>
                                    <td>- @money($item->pengeluaran)</td>
                                    <td>+ @money($item->pemasukan)</td>
                                    <td>-</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </section>
    
@endsection
@push('scripts')
<script src={{ asset ('assets/extensions/simple-datatables/umd/simple-datatables.js') }}></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script>
    let dataTable = new simpleDatatables.DataTable(
            document.getElementById("akuntansi")
        )

    var options = {
        colors: [ "#F8C300","#E27483"],
    series: [{
    name: 'Pemasukan',
    data: [
        {!! json_encode($incomeMonth[1], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[2], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[3], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[4], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[5], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[6], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[7], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[8], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[9], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[10], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[11], JSON_HEX_TAG) !!},
        {!! json_encode($incomeMonth[12], JSON_HEX_TAG) !!},
    ]
    
    }, {
    name: 'Pengeluaran',
    data: [
        {!! json_encode($outcomeMonth[1], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[2], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[3], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[4], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[5], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[6], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[7], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[8], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[9], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[10], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[11], JSON_HEX_TAG) !!},
        {!! json_encode($outcomeMonth[12], JSON_HEX_TAG) !!},
    ]
    }],
    chart: {
    height: 350,
    type: 'area'
    },
    dataLabels: {
    enabled: false
    },
    stroke: {
    curve: 'smooth'
    },
    xaxis: {
    type: 'datetime',
    categories: [
        "2023-01-01T00:00:00.000Z", 
        "2023-02-01T00:00:00.000Z", 
        "2023-03-01T00:00:00.000Z", 
        "2023-04-01T00:00:00.000Z", 
        "2023-05-01T00:00:00.000Z", 
        "2023-06-01T00:00:00.000Z", 
        "2023-07-01T00:00:00.000Z", 
        "2023-08-01T00:00:00.000Z", 
        "2023-09-01T00:00:00.000Z", 
        "2023-10-01T00:00:00.000Z", 
        "2023-11-01T00:00:00.000Z", 
        "2023-12-01T00:00:00.000Z"
        ]
    },
    tooltip: {
    x: {
        format: 'dd/MM/yy HH:mm'
    },
    },
    };

    var chart = new ApexCharts(document.querySelector("#keuanganChart"), options);
    chart.render();
</script>
@endpush
