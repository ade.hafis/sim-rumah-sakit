@extends('templates.master')
@section('title', 'Pembayaran / Riwayat Transaksi')
@section('page-name', 'Riwayat')
@push('styles')
@endpush
@section('content')
<div class="row ">
    <div class="col">
        <div class="card">
            <div class="card-content">
                <div class="card-body">

                    <div class="row">
                        <div class="m-1 p-3 d-flex justify-content-end">
                            <div class="p-1 rounded">
                                <!-- <input type="" class="form-control" placeholder="Search.."> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="">
                            <div class="table-responsive rounded border">
                                <table class="table table-xs justify-content-start ">
                                    <thead class="">
                                        <tr class="">
                                            <th class="fw-bold px-4">Tanggal</th>
                                            <th class="fw-bold px-4">Nama</th>
                                            <th class="fw-bold px-4">Total</th>
                                            <th class="fw-bold px-4 text-center">Status</th>
                                            <th class="fw-bold px-4 text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($invoice as $row)
                                            @php
                                                $no = 1;
                                            @endphp
                                        <tr class="">
                                            <td class="px-4">{{ $row->created_at}}</td>
                                            <td class="px-4">{{ $row->dkunjungan->kunjungan->pasien->nama}}</td>
                                            <td class="px-4">@money($row->dkunjungan->pembayaran)</td>
                                            <td class="px-4 text-center">
                                                <a
                                                    class="m-1 rounded bg-success btn p-1 px-4 text-center fw-bolder text-white">
                                                    SUCCESS
                                                </a>
                                            </td>
                                            <td class="px-4 text-center">
                                                <a class="m-1 rounded bg-primary p-1 btn px-4 text-center fw-bolder text-white"
                                                    data-bs-toggle="modal" data-bs-target="#paymentSuccessDetail{{$row->id}}">
                                                    DETAILS
                                                </a>
                                                <div class="modal fade modal-lg" id="paymentSuccessDetail{{$row->id}}" tabindex="-1"
                                                    aria-labelledby="paymentSuccessDetail{{$row->id}}Title" aria-hidden="true"
                                                    style="display: none;">
                                                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="paymentSuccessDetail{{$row->id}}Title">
                                                                    Reciept Pasien / {{ $row->dkunjungan->kunjungan->pasien->nama}}</h5>
                                                                <button type="button" class="close"
                                                                    data-bs-dismiss="modal" aria-label="Close">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                        height="24" viewBox="0 0 24 24" fill="none"
                                                                        stroke="currentColor" stroke-width="2"
                                                                        stroke-linecap="round" stroke-linejoin="round"
                                                                        class="feather feather-x">
                                                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <div class="">
                                                                    <div class="d-flex justify-content-start m-4">
                                                                        

                                                                        <div>INVOICE/RSPEMNS/{{ strtotime($row->created_at )}}/1290{{$row->id}}</div>
                                                                    </div>
                                                                    <div class="table-responsive rounded border m-4">
                                                                    <table
                                                                                class="table table-xs justify-content-start ">
                                                                                <thead class="">
                                                                                <tr class="">
                                                                                    <th class="fw-bold px-4">Order</th>
                                                                                    <th class="fw-bold px-4 text-start">
                                                                                        Keterangan
                                                                                    </th>
                                                                                    <th class="fw-bold px-4">Desc</th>
                                                                                    <th
                                                                                        class="fw-bold px-4 text-center">
                                                                                        @harga
                                                                                    </th>
                                                                                    <th
                                                                                        class="fw-bold px-4 text-center">
                                                                                        Qty
                                                                                    </th>
                                                                                    <th
                                                                                        class="fw-bold px-4 text-center">
                                                                                        @Total
                                                                                    </th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody class="fw-normal">
                                                                                @if(isset($row->dkunjungan->apotek_id))

                                                                                    <tr>
                                                                                        <th colspan="2"
                                                                                            class="p-2 fw-bolder ">
                                                                                            <div
                                                                                                style="padding: 0 0 0 80px; border-bottom: 0px;"
                                                                                                class="text-start">
                                                                                                Drugs
                                                                                            </div>
                                                                                        </th>
                                                                                    </tr>
                                                                                    
                                                                                    @foreach($apotek->where('prescription_id', $row->dkunjungan->invoiceResep->id) as $items)

                                                                                    @php
                                                                                        $dataObat = $drugs->where('id',$items->drug_id)->first();
                                                                                        
                                                                                    @endphp
                                                                                    <tr class="fw-normal">
                                                                                        
                                                                                        <th class="fw-normal">{{$loop->iteration}}</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            {{ $dataObat->name}}
                                                                                        </th>
                                                                                        <th class="fw-normal">-
                                                                                        </th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($dataObat->price)
                                                                                        </th>
                                                                                        <th class="fw-normal">{{$items->quantity}}</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($dataObat->price * $items->quantity)
                                                                                        </th>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                @endif

                                                                                <tr>
                                                                                    <th colspan="2"
                                                                                        class="p-2 fw-bolder">
                                                                                        <div
                                                                                            style="padding: 0 0 0 80px;"
                                                                                            class="text-start">
                                                                                            Services
                                                                                        </div>
                                                                                    </th>
                                                                                </tr>

                                                                                <tr class="">
                                                                                    <th class="fw-normal">{{$loop->iteration}}</th>
                                                                                    <th class="fw-normal px-4 text-start">
                                                                                        Biaya Registrasi Rumah Sakit
                                                                                    </th>
                                                                                    <th class="fw-normal">Rawat Jalan
                                                                                    </th>
                                                                                    <th class="fw-normal px-4 text-start">
                                                                                        Rp. 30.000,00
                                                                                    </th>
                                                                                    <th class="fw-normal">1</th>
                                                                                    <th class="fw-normal px-4 text-start">
                                                                                        Rp. 30.000,00
                                                                                    </th>
                                                                                </tr>
                                                                                @if($lab->where('kunjungan_id',$row->dkunjungan->kunjungan->id) !== null)
                                                                                    <tr>
                                                                                        <th colspan="2"
                                                                                            class="p-2 fw-bolder">
                                                                                            <div
                                                                                                style="padding: 0 0 0 80px;"
                                                                                                class="text-start">
                                                                                                Labs
                                                                                            </div>
                                                                                        </th>
                                                                                    </tr>
                                                                                    @foreach($lab->where('kunjungan_id',$row->dkunjungan->kunjungan->id) as $res)
                                                                                    <tr class="">
                                                                                        <th class="fw-normal">{{$loop->iteration}}</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            {{$res->lab->name}}
                                                                                        </th>
                                                                                        <th class="fw-normal">
                                                                                           {{$res->description ?? '-'}}
                                                                                        </th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($res->lab->price)
                                                                                        </th>
                                                                                        <th class="fw-normal">1</th>
                                                                                        <th class="fw-normal px-4 text-start">
                                                                                            @money($res->lab->price)
                                                                                        </th>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                @endif
                                                                                </tbody>
                                                                            </table>   

                                                                    </div>
                                                                    <div class="border mt-4 rounded m-4">
                                                                        <div class="d-flex m-1 justify-content-between p-2">
                                                                            <div class="text-start">
                                                                                <div style="font-size: 12px;">- PT.PEMNS NPWP : 213131000</div>
                                                                                <div style="font-size: 12px;">- JIKA TERDAPAT KESALAHAN DAPAT
                                                                                    MENGHUBUNGI KAMI</div>
                                                                            </div>
                                                                            <div class="text-start d-flex fw-bold" style="margin: 0 35px 0 0;">
                                                                                <div class="mx-4">Total Biaya</div>
                                                                                <div class="mx-2">@money($row->dkunjungan->pembayaran)</div>    
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-light-secondary"
                                                                    data-bs-dismiss="modal">
                                                                    <i class="bx bx-x d-block d-sm-none"></i>
                                                                    <span class="d-none d-sm-block fw-bold">CLOSE</span>
                                                                </button>
                                                                <a  class="btn btn-dark ms-1" href="{{ route('pembayaran-riwayat-cetak') }}/{{$row->id}}"
                                                                    >
                                                                    <i class="bx bx-check d-block d-sm-none"></i>
                                                                    <span class="d-none d-sm-block fw-bold">CETAK</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="mt-5 d-flex justify-content-between">
                            <ul>
                                <p>Showing <select name="" id="" disabled>
                                        <option value="15" class="disabled" selected>10</option>
                                    </select> results of {{ $invoice->total()}}</p>
                            </ul>
                            @if($invoice->hasPages())
                            <ul class="pagination pagination-primary  justify-content-end">
                                @if($invoice->onFirstPage())
                                <li class="page-item disabled">
                                    <a class="page-link" href="{{ $invoice->previousPageUrl() }}" tabindex="-1" aria-disabled="true">Previous</a>
                                </li>
                                @else
                                <li class="page-item">
                                    <a class="page-link" href="{{ $invoice->previousPageUrl() }}" tabindex="-1" aria-disabled="true">Previous</a>
                                </li>
                                @endif

                                @foreach($invoice->getUrlRange(1,$invoice->lastPage()) as $i=>$element)
                               
                                @if (is_string($element)) 
                                <li class="page-item mx-1"><a class="page-link" href="{{ $element }}">{{ $i }}</a></li>    
                                @endif

                                @if (is_array($element)) 
                                @foreach ($element as $page => $url) 
                                
                                @if ($page == $invoice->currentPage()) 
                                    <li class="page-item active"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                @else
                                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                                @endforeach 
                                @endif 
                                @endforeach 
                                @if ($invoice->hasMorePages()) 
                                    <li class="page-item">
                                        <a class="page-link" href="{{ $invoice->nextPageUrl() }}">Next</a>
                                    </li>
                                    @else
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#">Next</a>
                                    </li>
                                    @endif
                                </ul>
                            @endif
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush